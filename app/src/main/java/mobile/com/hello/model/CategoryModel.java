package mobile.com.hello.model;

import java.util.ArrayList;

import mobile.com.hello.models.RssModel;

/**
 * Created by reflex on 30/9/17.
 */

public class CategoryModel {

    private static CategoryModel INSTANCE = null;

    private CategoryModel(){

    }

    public static CategoryModel getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new CategoryModel();
        }
        return(INSTANCE);
    }


    public ArrayList<RssModel> getHealthList() {
        return healthList;
    }

    public void setHealthList(ArrayList<RssModel> healthList) {
        if(this.healthList!=null && !this.healthList.isEmpty()){
            this.healthList.clear();
            this.healthList = healthList;
        }else{
            this.healthList = healthList;
        }

    }

    public ArrayList<RssModel> getExposeList() {
        return exposeList;
    }

    public void setExposeList(ArrayList<RssModel> exposeList) {
        this.exposeList = exposeList;
    }

    public ArrayList<RssModel> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(ArrayList<RssModel> historyList) {
        this.historyList = historyList;
    }

    public ArrayList<RssModel> getNewsList() {
        return newsList;
    }

    public void setNewsList(ArrayList<RssModel> newsList) {
        this.newsList = newsList;
    }

    public ArrayList<RssModel> getRrgList() {
        return rrgList;
    }

    public void setRrgList(ArrayList<RssModel> rrgList) {
        this.rrgList = rrgList;
    }

    public ArrayList<RssModel> getIncredibleList() {
        return incredibleList;
    }

    public void setIncredibleList(ArrayList<RssModel> incredibleList) {
        this.incredibleList = incredibleList;
    }

    public ArrayList<RssModel> getOrganicFarmingList() {
        return organicFarmingList;
    }

    public void setOrganicFarmingList(ArrayList<RssModel> organicFarmingList) {
        this.organicFarmingList = organicFarmingList;
    }

    public ArrayList<RssModel> getHomeMadeList() {
        return homeMadeList;
    }

    public void setHomeMadeList(ArrayList<RssModel> homeMadeList) {
        this.homeMadeList = homeMadeList;
    }

    public ArrayList<RssModel> healthList;
    public ArrayList<RssModel> exposeList;
    public ArrayList<RssModel> historyList;
    public ArrayList<RssModel> newsList;
    public ArrayList<RssModel> rrgList;
    public ArrayList<RssModel> incredibleList;
    public ArrayList<RssModel> organicFarmingList;
    public ArrayList<RssModel> homeMadeList;
}
