package mobile.com.hello.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.InputStream;

import mobile.com.hello.R;

/**
 * Created by Honey on 13/Aug/2017.
 */

public class AudioDialog {
    Context context;
    Context activityContext;
    LayoutInflater inflater;
    TextView textView,title;
    int dialogId;
    String fileName,titleText ;
    Button  dismis;


    public Dialog gotoDialog() {

        // custom dialog
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activityContext);
        View dialogView = inflater.inflate(dialogId, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
/**        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {



            }
        });*/
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();
        textView = (TextView) dialogView.findViewById(R.id.text);
        textView.setText(readFromFile(context, fileName));

        title = (TextView) dialogView.findViewById(R.id.title);
        title.setText(titleText);

        dismis = (Button) dialogView.findViewById(R.id.dismis);
        dismis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
            }
        });

        return alertDialog;
    }



    public static String readFromFile(Context context, String file) {
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte buffer[] = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public AudioDialog(Context context, Context activityContext, LayoutInflater inflater,int dialogId,String fileName,String titleText) {

        this.context = context;
        this.activityContext = activityContext;
        this.inflater = inflater;
        this.dialogId = dialogId;
        this.fileName = fileName;
        this.titleText =titleText;
    }

}
