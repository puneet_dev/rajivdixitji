package mobile.com.hello.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import mobile.com.hello.R;
import mobile.com.hello.util.DownloaderUtil;

/**
 * Created by Honey on 13/Aug/2017.
 */

public class DownloadDialog {
    Context context;
    Context activityContext;
    LayoutInflater inflater;
    TextView name, size;
    int dialogId;
    String fileName;
    Button cancel, download;
    String link, fileSize = "", extension;


    public Dialog gotoDialog() {

        // custom dialog
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activityContext);
        View dialogView = inflater.inflate(dialogId, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();


        name = (TextView) dialogView.findViewById(R.id.file_name);
        name.setText(fileName + extension);

        size = (TextView) dialogView.findViewById(R.id.file_size);
        size.setText("Size: xx.xx MB");
        new getSize().execute();

        cancel = (Button) dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
            }
        });


        download = (Button) dialogView.findViewById(R.id.download);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DownloaderUtil.file_download(activityContext, link, fileName, "", extension);
                Toast.makeText(activityContext,"Download Started...",Toast.LENGTH_LONG).show();
                alertDialog.dismiss();

            }
        });

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return alertDialog;
    }


//    public String downloadFileSize(final String link) {
//
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
////                int length = 0;
////                URL url = null;
////                URLConnection connection = null;
////                try {
////                    url = new URL(link);
////                    connection = url.openConnection();
////                    length = connection.getContentLength();
////
////                } catch (IOException e) {
////                }
////
////                if (length < 1024) {
////                    fileSize = length + " KB";
////                } else {
////                    fileSize = (length / 1024) + ("." + (length % 1024)).substring(0, 2) + " MB";
////
////                }
//            }
//        });
//        return "Size: " + fileSize;
//    }

    public DownloadDialog(Context context, Context activityContext, LayoutInflater inflater, int dialogId, String link, String fileName, String extension) {

        this.context = context;
        this.activityContext = activityContext;
        this.inflater = inflater;
        this.dialogId = dialogId;
        this.fileName = fileName;
        this.link = link;
        this.extension = extension;
    }


    private class getSize extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            int length = 0;
            URL url = null;
            URLConnection connection = null;
            try {
                url = new URL(link);
                connection = url.openConnection();
                length = connection.getContentLength();

            } catch (IOException e) {
            }
            if (length<1024) {
                fileSize = length  + "B";
            } else if ((length / 1024) < 1024) {
                fileSize = (length / 1024) + " KB";
            } else {
                fileSize = (length / 1024) / 1024 + "." +((length / 1024) % 1024+"000").substring(0, 2) + " MB";

            }
            return "Size: " + fileSize;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(final String filesize) {
            super.onPostExecute(filesize);

            size.setText(filesize);
        }

    }

}