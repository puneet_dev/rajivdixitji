package mobile.com.hello.interfaces;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.models.RssModel;

/**
 * Created by desk52 on 27/7/16.
 */
public interface IAsyncTask {
   Context getContext();
   Fragment getFragment();
   void OnPreExecute();
   void OnPostExecute(String URL, List<RssFeedModel> result);
   void OnPostExecute(ArrayList<String> result);
   void OnPostExecute(String message, ArrayList<RssModel> result);
   void OnErrorMessage(String Message);
}
