package mobile.com.hello.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import mobile.com.hello.R;
import mobile.com.hello.activities.HomeActivity;
import mobile.com.hello.activities.IntroActivity;
import mobile.com.hello.models.RssModel;

/**
 * Created by puneet on 3/8/17.
 */

public class DetailFragment extends Fragment {
    Context context;
    RssModel postModel;
    TextView postCategoryText;
    TextView postContent;
    String contentString;
    ImageView detailImage;
    String postCategory;
    JustifiedTextView postTitle;

    private static final String ARG_SECTION_NUMBER = "section_number";

    public void setData(Context context, RssModel postModel, String postCategory) {
        this.context = context;
        this.postModel = postModel;
        this.postCategory = postCategory;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_fragment, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        postTitle = (JustifiedTextView) v.findViewById(R.id.detail_title);

        detailImage = (ImageView) v.findViewById(R.id.detail_image);
        contentString = postModel.getContent();
        contentString = contentString.replace("[\"The post\",\"appeared first on\",\".\"]", " ");
        contentString = contentString.replace("[", " ");
        contentString = contentString.replace("]", " ");
        postTitle.setText(postModel.getTitle());

        this.context = getActivity();

        postContent.setText(contentString);
        postCategoryText.setText(postCategory);
        Picasso.with(context)
                .load(postModel.getImagePath())
                .into(detailImage);
        return v;
    }
}
