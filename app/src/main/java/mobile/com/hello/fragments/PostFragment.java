package mobile.com.hello.fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.FeedAsync;
import mobile.com.hello.R;
import mobile.com.hello.adapter.DatabaseAdapter;
import mobile.com.hello.adapter.RssAdapter;
import mobile.com.hello.asynctask.ConvertAsyncTask;
import mobile.com.hello.entity.UserInfo;
import mobile.com.hello.interfaces.IAsyncTask;
import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.models.RssModel;
import mobile.com.hello.util.Constants;
import mobile.com.hello.util.Util;

import static mobile.com.hello.util.Constants.BHARAT_TABLE_NAME;
import static mobile.com.hello.util.Constants.EXPOSE_TABLE_NAME;
import static mobile.com.hello.util.Constants.FARMING_TABLE_NAME;
import static mobile.com.hello.util.Constants.HEALTH_TABLE_NAME;
import static mobile.com.hello.util.Constants.HISTORY_TABLE_NAME;
import static mobile.com.hello.util.Constants.NEWS_TABLE_NAME;
import static mobile.com.hello.util.Constants.RECALL_TABLE_NAME;
import static mobile.com.hello.util.Constants.SWADESHI_TABLE_NAME;

/**
 * Created by puneet on 3/8/17.
 */

public class PostFragment extends Fragment implements IAsyncTask {
    private Context context;
    List<String> rssEnteries = null;
    RecyclerView healthRecyclerView;
    String category, url;
    ArrayList<RssModel> categoryData;
    DatabaseAdapter databaseAdapter;
    static int position=0;

    public void setData(String feedUrl, String category){
        this.url = feedUrl;
        this.category = category;
    }

    public static void setFragmentPosition(int Position){
        position = position;
    }

    public void setData(ArrayList<RssModel> categoryData)   {
        this.categoryData = categoryData;
    }

    /*public static PostFragment newInstance(ArrayList<RssModel> categoryArrayList) {
        
        Bundle args = new Bundle();
        
        PostFragment fragment = new PostFragment();

        args.putParcelableArrayList("categoryArrayList",categoryArrayList);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.post_fragment,container,false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        context = getActivity();
        rssEnteries = new ArrayList<>();
        healthRecyclerView = (RecyclerView) v.findViewById(R.id.health_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        healthRecyclerView.setLayoutManager(linearLayoutManager);

        databaseAdapter = new DatabaseAdapter(context);

        /*if(categoryData==null){
            categoryData= databaseAdapter.getCategoryListFromDb(HEALTH_TABLE_NAME);
        }*/

            if(categoryData!=null && categoryData.size()>0){
                RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                healthRecyclerView.setAdapter(rssAdapter);
            } else{
                if(position==0){
                    categoryData = databaseAdapter.getCategoryListFromDb(HEALTH_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else if(position==1){
                    categoryData = databaseAdapter.getCategoryListFromDb(EXPOSE_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else if(position==2){
                    categoryData = databaseAdapter.getCategoryListFromDb(HISTORY_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else if(position==3){
                    categoryData = databaseAdapter.getCategoryListFromDb(NEWS_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else if(position==4){
                    categoryData = databaseAdapter.getCategoryListFromDb(RECALL_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else if(position==5){
                    categoryData = databaseAdapter.getCategoryListFromDb(FARMING_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else if(position==6){
                    categoryData = databaseAdapter.getCategoryListFromDb(BHARAT_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else if(position==7){
                    categoryData = databaseAdapter.getCategoryListFromDb(SWADESHI_TABLE_NAME);
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }
                /*if(getArguments().getParcelableArrayList("categoryArrayList")!=null && getArguments().getParcelableArrayList("categoryArrayList").size()>0){
                    categoryData = getArguments().getParcelableArrayList("categoryArrayList");
                    RssAdapter rssAdapter = new RssAdapter(context, categoryData, category);
                    healthRecyclerView.setAdapter(rssAdapter);
                }else{
                    if(Util.haveNetworkConnection(context)){
                        parseFeed();
                    }else{
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }*/

                /*if(Util.haveNetworkConnection(context)){
                    parseFeed();
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }*/
            }

        return v;
    }

    private void parseFeed() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUrl(url);
        FeedAsync feedAsync = new FeedAsync(userInfo, this);
        feedAsync.execute();
    }

    @Override
    public Fragment getFragment() {
        return null;
    }

    @Override
    public void OnPreExecute() {
        Util.showDialog(context);
    }

    @Override
    public void OnPostExecute(String URL, List<RssFeedModel> result) {
        if(result!=null){
            Log.d("result", result.toString());
            UserInfo userInfo = new UserInfo();
            userInfo.setResult(result);
            ConvertAsyncTask convertAsyncTask = new ConvertAsyncTask(userInfo, this);
            convertAsyncTask.execute();
        }else{
            Toast.makeText(context, "No Post available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void OnPostExecute(ArrayList<String> result) {

    }

    @Override
    public void OnPostExecute(String message, ArrayList<RssModel> result) {
        Util.dismissDialog();
        try{
            if(result!=null && result.size()>0){
                if(category.equalsIgnoreCase(Constants.getInstance().healthString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  HEALTH_TABLE_NAME);
                        }

                }else if(category.equalsIgnoreCase(Constants.getInstance().exposeString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  EXPOSE_TABLE_NAME);
                        }

                }else if(category.equalsIgnoreCase(Constants.getInstance().realTruthString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  HISTORY_TABLE_NAME);
                        }

                }else if(category.equalsIgnoreCase(Constants.getInstance().swadeshiString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  NEWS_TABLE_NAME);
                        }

                }else if(category.equalsIgnoreCase(Constants.getInstance().rightCallString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  RECALL_TABLE_NAME);
                        }

                }else if(category.equalsIgnoreCase(Constants.getInstance().incredibleString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  BHARAT_TABLE_NAME);
                        }

                }else if(category.equalsIgnoreCase(Constants.getInstance().organicString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  FARMING_TABLE_NAME);
                        }

                }else if(category.equalsIgnoreCase(Constants.getInstance().productString)){

                        for(int i=0; i<result.size();i++){
                            databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(),  SWADESHI_TABLE_NAME);
                        }
                }
                RssAdapter rssAdapter = new RssAdapter(context, result, category);
                healthRecyclerView.setAdapter(rssAdapter);
            }
            else{
                Toast.makeText(context, "No Data Available", Toast.LENGTH_SHORT).show();
            }

        }catch(NullPointerException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void OnErrorMessage(String Message) {

    }
}
