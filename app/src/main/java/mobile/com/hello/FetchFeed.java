package mobile.com.hello;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.models.RssFeedModel;

public class FetchFeed {

    public List<RssFeedModel> parseFeed(InputStream inputStream) throws XmlPullParserException, IOException {
        String title = null;
        String link = null;
        String description = null;
        String content = null;
        boolean isItem = false;
        List<RssFeedModel> items = new ArrayList<>();

        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        xmlPullParser.setInput(inputStream, null);
        xmlPullParser.nextTag();

        while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
            int eventType = xmlPullParser.getEventType();
            String name = xmlPullParser.getName();
            if (name == null)
                continue;
            if (eventType == XmlPullParser.END_TAG) {
                if (name.equalsIgnoreCase("item")) {
                    isItem = false;
                }
                continue;
            }
            if (eventType == XmlPullParser.START_TAG) {
                if (name.equalsIgnoreCase("item")) {
                    isItem = true;
                    continue;
                }
            }
            String result = "";
            if (xmlPullParser.next() == XmlPullParser.TEXT) {
                result = xmlPullParser.getText();
                xmlPullParser.nextTag();
            }
            if (name.equalsIgnoreCase("title")) {
                title = result;
            } else if (name.equalsIgnoreCase("link")) {
                link = result;
            } else if (name.equalsIgnoreCase("description")) {
                description = result;
            } else if (name.equalsIgnoreCase("content:encoded")) {
                content = result;
            }
            if (title != null && link != null && description != null && content != null) {
                if (isItem) {
                    RssFeedModel item = new RssFeedModel(title, link, description, content);
                    items.add(item);
                    if (items.size() > 20) {
                        break;
                    }
                } else {
                    /*mFeedTitle = title;
                    mFeedLink = link;
                    mFeedDescription = description;*/
                }

                title = null;
                link = null;
                description = null;
                content = null;
                isItem = false;
            }
        }
        inputStream.close();
        return items;
    }


}
