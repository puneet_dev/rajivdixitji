package mobile.com.hello.asynctask;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.entity.UserInfo;
import mobile.com.hello.interfaces.IAsyncTask;
import mobile.com.hello.models.RssModel;
import mobile.com.hello.util.ResultSet;

/**
 * Created by reflex on 26/7/17.
 */

public class ConvertAsyncTask extends AsyncTask<Object, String, ResultSet> {
    UserInfo userInfo;
    IAsyncTask iAsyncTask;
    List<RssFeedModel> result;
    ArrayList<String> rssEnteries = null;
    ArrayList<RssModel> rssArrayList;
    String postTitle, postLink, postContent, imagePath;
    RssModel rssModel;

    public ConvertAsyncTask(UserInfo usersInfo, IAsyncTask iAsyncTask) {
        this.userInfo = usersInfo;
        this.iAsyncTask = iAsyncTask;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ResultSet doInBackground(Object... objects) {
        result = new ArrayList<>();
        rssEnteries = new ArrayList<>();
        rssArrayList = new ArrayList<>();
        result = userInfo.getResult();

        for (int i = 0; i < result.size(); i++) {
//            rssModel = new RssModel(postTitle, postContent, postLink, imagePath);
            rssModel = new RssModel();
            String contentData = result.get(i).content;
            contentData = contentData.replaceAll("&nbsp;", " ");
            postTitle = result.get(i).title.toString();
            postLink = result.get(i).link.toString();
            imagePath = "";
            JSONObject jsonObj = null;
            try {
                jsonObj = XML.toJSONObject(contentData);
                if (jsonObj.has("img")) {
                    String imageTag = jsonObj.get("img").toString();
                    JSONObject obj = new JSONObject(imageTag);
                    imagePath = obj.get("src").toString();
                }
                postContent = "";
                if (jsonObj.has("p")) {
                    JSONArray pTag = (JSONArray) jsonObj.get("p");
                    for (int j = 0; j < pTag.length(); j++) {
                        if (pTag.get(j) instanceof String) {
                            //if (postContent.equalsIgnoreCase("")) {
                                postContent += pTag.get(j);

                            //}
                            continue;
                        }
                        JSONObject pTagObject = (JSONObject) pTag.get(j);
                        if (pTagObject.has("content")) {
                            postContent += pTagObject.get("content").toString()+"\n";
                        }
                        if (pTagObject.has("img") && imagePath.equalsIgnoreCase("")) {
                            JSONObject imageObject = (JSONObject) pTagObject.get("img");
                            imagePath = (String) imageObject.get("src");
                        }
                    }
                }

                if (postContent.length() != 0 && !postContent.equalsIgnoreCase("")) {
                    rssModel.setContent(postContent);
                }
                if (postLink != null && !postLink.equalsIgnoreCase("")) {
                    rssModel.setPostLink(postLink);
                }
                if (postTitle != null && !postTitle.equalsIgnoreCase("")) {
                    rssModel.setTitle(postTitle);
                }
                if (imagePath != null && !imagePath.equalsIgnoreCase("")) {
                    rssModel.setImagePath(imagePath);
                }

                if (rssModel != null) {
                    rssModel.setIsClick("false");
                    rssArrayList.add(rssModel);
                }

//                rssEnteries.add(imagePath);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                //error
            }
        }
        ResultSet resultSet = new ResultSet();
//        resultSet.setImage(rssEnteries);
        resultSet.setRssModelArrayList(rssArrayList);
        return resultSet;
    }

    @Override
    protected void onPostExecute(ResultSet resultSet) {
        super.onPostExecute(resultSet);

//        iAsyncTask.OnPostExecute(resultSet.getImage());
        iAsyncTask.OnPostExecute("success", resultSet.getRssModelArrayList());
    }
}
