package mobile.com.hello.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.interfaces.IAsyncTask;
import mobile.com.hello.models.RssModel;

public class WebViewActivity extends AppCompatActivity implements IAsyncTask {

    WebView webView;
    String content;
    IAsyncTask iAsyncTask;
    ArrayList<String> contentArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        webView = (WebView) findViewById(R.id.web_view);

        contentArrayList = new ArrayList<>();
        contentArrayList = getIntent().getStringArrayListExtra("content");
        content = contentArrayList.get(0);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().getLoadsImagesAutomatically();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.getSettings().setSupportZoom(false);
        webView.loadData(content,"text/html; charset=utf-8","UTF-8");
//        webView.loadUrl(url);







    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public Fragment getFragment() {
        return null;
    }

    @Override
    public void OnPreExecute() {

    }

    @Override
    public void OnPostExecute(String URL, List<RssFeedModel> result) {
        if(result!=null) {
            Log.d("webview result", result.toString());
        }
    }

    @Override
    public void OnPostExecute(ArrayList<String> result) {

    }

    @Override
    public void OnPostExecute(String message, ArrayList<RssModel> result) {

    }

    @Override
    public void OnErrorMessage(String Message) {

    }
}
