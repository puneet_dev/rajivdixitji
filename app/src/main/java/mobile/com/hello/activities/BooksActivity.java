package mobile.com.hello.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.adapter.BooksAdapter;
import mobile.com.hello.dialog.AudioDialog;
import mobile.com.hello.dialog.DownloadDialog;
import mobile.com.hello.models.BooksItem;
import mobile.com.hello.util.NetworkUtils;
import mobile.com.hello.util.ReadWriteStorage;
import mobile.com.hello.util.SharedPref;

import static android.os.Environment.getExternalStoragePublicDirectory;


public class BooksActivity extends AppCompatActivity implements BooksAdapter.BooksItemClick {

    RecyclerView recyclerView;
    private BooksAdapter booksAdapter;
    TextView title;
    List<BooksItem> list;
    String downloadFileName;
    LayoutInflater inflater;
    boolean firstlaunch = false;


    int position;

    CoordinatorLayout coordinatorLayout;
    static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 0;
    private static final int REQUEST_APP_SETTINGS = 168;
    Context appContext, activityContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main);
        appContext = getApplicationContext();
        activityContext = BooksActivity.this;


        list = new ArrayList<>();

        title = (TextView) findViewById(R.id.title);
        title.setText("\uD83D\uDCD6" + " स्वदेशी पुस्तकालय " + "\uD83D\uDCD6");


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        booksAdapter = new BooksAdapter(getBooksListData(), this);
        booksAdapter.setOnclickListner(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(booksAdapter);

        inflater = getLayoutInflater();


        //read shared preferences for first launch
        SharedPref sharedPref = new SharedPref(appContext);
        if (sharedPref.readBoolean("firstLaunchBooks")) {
            inflater = getLayoutInflater();
            new AudioDialog(this, activityContext, inflater, R.layout.dialog_audio, "booksintro.txt", "राजीव भाई  की लिखित पुस्तके").gotoDialog().show();
            sharedPref.writeBoolean("firstLaunchBooks", false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstlaunch) {
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        }

    }

    @Override
    public void onClick(View v, int position) {

        this.position = position;
        if (NetworkUtils.isConnected(activityContext)) {


            if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(BooksActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                onPermissionGranted();


            } else if (ContextCompat.checkSelfPermission(BooksActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

                ActivityCompat.requestPermissions(BooksActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

            }


        } else {
            if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(BooksActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                onPermissionGranted();


            } else {

                Toast.makeText(activityContext, "No  Internet Connection", Toast.LENGTH_LONG).show();

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    onPermissionGranted();

                } else {

                    Snackbar snackbar;
                    snackbar = Snackbar.make(coordinatorLayout, "Storage Permission Required for Books Downloading", Snackbar.LENGTH_LONG);
                    TextView action = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_action);
                    action.setTextSize(16);
                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setAction("Settings", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openSettings();
                        }
                    });
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ContextCompat.getColor(appContext, R.color.colorPrimary));
                    snackbar.show();


                }


            }
        }
    }

    private void openSettings() {

        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);

    }

    public void onPermissionGranted() {
        BooksItem item = list.get(position);
        downloadFileName = item.getName() + ".pdf";
        if (downloadedFileFound(ReadWriteStorage.readFileInfo("", ".pdf"))) {
            openPdfFile();
        } else {


            downloadPDF(position);

        }

    }

    public List<BooksItem> getBooksListData() {

        try {
            String jsonLocation = readFromFile(this, "bookslinksfinal.json");

            JSONObject jsonobject = new JSONObject(jsonLocation);
            JSONArray jsonArray = jsonobject.getJSONArray("book");

            BooksItem booksItem;

            for (int i = 0; i < jsonArray.length(); i++) {
                booksItem = new BooksItem();

                JSONObject json = jsonArray.getJSONObject(i);


                booksItem.setLink(json.getString("link"));
                booksItem.setName(json.getString("name"));
                list.add(booksItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    public static String readFromFile(Context context, String file) {
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte buffer[] = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void downloadPDF(int position) {


        if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            //do your work here
            BooksItem item = list.get(position);
            downloadFileName = item.getName();

            new DownloadDialog(this, activityContext, inflater, R.layout.dialog_download, item.getLink(), downloadFileName, ".pdf").gotoDialog().show();


        } else if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

            ActivityCompat.requestPermissions(BooksActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

        }
    }

    public boolean downloadedFileFound(String[] downloadedFiles) {

        boolean fileFound = false;
        for (int i = 0; i < downloadedFiles.length; i++) {
            if (downloadFileName.equals(downloadedFiles[i])) {

                fileFound = true;
                break;

            }

        }
        return fileFound;
    }

    public void openPdfFile() {
        //boolean for animation on resume
        firstlaunch = true;
        File file = new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/RajivDixitJi/" + downloadFileName);

        try {
            Intent googleDrive = new Intent(Intent.ACTION_VIEW);
            googleDrive.setDataAndType(Uri.fromFile(file), "application/pdf");
            googleDrive.setPackage("com.google.android.apps.docs");
            activityContext.startActivity(googleDrive);
            ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

        } catch (Exception ex) {
            try {
                Intent send = new Intent(Intent.ACTION_VIEW);
                send.setDataAndType(Uri.fromFile(file), "application/pdf");
                activityContext.startActivity(send);
                ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

            } catch (Exception e) {
                Toast.makeText(this, "You Don't Have any Pdf Viewer Installed Please install Pdf Viewer", Toast.LENGTH_LONG).show();
            }

        }

    }

}