package mobile.com.hello.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import mobile.com.hello.R;
import mobile.com.hello.fragments.PostFragment;

public class CategoryActivity extends AppCompatActivity {

    PostFragment postFragment;
    FragmentManager manager;
    String category, url;
    public Toolbar toolbar;
    ImageView backImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        toolbar = (Toolbar) findViewById(R.id.toolbar_category);
        backImage = (ImageView) findViewById(R.id.back_button);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        try{
            url = getIntent().getExtras().get("url").toString();
            category = getIntent().getExtras().get("category").toString();

            if(!url.equalsIgnoreCase("") && url!=null){
                postFragment = new PostFragment();
                postFragment.setData(url, category);
                pushHomeFragment(postFragment);
            }

        }catch(NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void pushHomeFragment(Fragment fragment) {

        manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.activity_category, fragment);
        transaction.commit();
    }
}
