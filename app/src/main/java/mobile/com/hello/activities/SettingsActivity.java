package mobile.com.hello.activities;

import android.content.Context;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

import mobile.com.hello.R;
import mobile.com.hello.util.SharedPref;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class SettingsActivity extends AppCompatActivity {


    boolean firstlaunch = false;
    Toolbar toolbar;
    ImageView backImage;
    CheckBox sync, read, unread, starred;
    Context context;
    SharedPref sharedPref;

    private AdView adView;
    LinearLayout adContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        context = SettingsActivity.this;
        sharedPref = new SharedPref(context);

        toolbar = (Toolbar) findViewById(R.id.toolbarwebpage_intro);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        backImage = (ImageView) findViewById(R.id.back_button);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //initilize facebook banner ads
        adView = new AdView(this, getResources().getString(R.string.facebook_settings_banner_id), AdSize.BANNER_HEIGHT_50);
        // Find the Ad Container
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        // Add the ad view to your activity layout
        adContainer.addView(adView);
        // Request an ad
        adView.loadAd();
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                adView.loadAd();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

        });



        sync = (CheckBox) findViewById(R.id.check_sync);
        sync.setChecked(sharedPref.readBoolean("sync"));
        sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sync.isChecked()) {
                    sharedPref.writeBoolean("sync", TRUE);
                } else {

                    new SharedPref(context).writeBoolean("sync", FALSE);
                }
            }
        });

        read = (CheckBox) findViewById(R.id.check_read);
        read.setChecked(sharedPref.readBoolean("read"));
        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (read.isChecked()) {
                    sharedPref.writeBoolean("read", TRUE);
                } else {

                    new SharedPref(context).writeBoolean("read", FALSE);
                }
            }
        });


        unread = (CheckBox) findViewById(R.id.check_unread);
        unread.setChecked(sharedPref.readBoolean("unread"));
        unread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unread.isChecked()) {
                    sharedPref.writeBoolean("unread", TRUE);
                } else {

                    new SharedPref(context).writeBoolean("unread", FALSE);
                }
            }
        });


        starred = (CheckBox) findViewById(R.id.check_starred);
        starred.setChecked(sharedPref.readBoolean("starred"));
        starred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (starred.isChecked()) {
                    sharedPref.writeBoolean("starred", TRUE);
                } else {

                    new SharedPref(context).writeBoolean("starred", FALSE);
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstlaunch) {
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        }

    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

}