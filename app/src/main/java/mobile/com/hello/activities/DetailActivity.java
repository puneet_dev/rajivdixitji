package mobile.com.hello.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

import mobile.com.hello.R;
import mobile.com.hello.adapter.DatabaseAdapter;
import mobile.com.hello.adapter.ViewPagerDetailAdapter;
import mobile.com.hello.models.RssModel;
import mobile.com.hello.util.ZoomOutPageTransformer;

import static mobile.com.hello.R.id.viewPagerDetail;

public class DetailActivity extends AppCompatActivity implements ViewPagerDetailAdapter.DetailItemClick {
    public ViewPager viewPager;
    ArrayList<RssModel> postArrayList;
    String currenPosition, postUrl;
    ViewPagerDetailAdapter viewPagerDetailAdapter;
    ImageButton shareIcon;
    Context context;
    DatabaseAdapter databaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeActivityFullScreen();
        setContentView(R.layout.activity_detail);
        initializeId();

        this.context = this;

        databaseAdapter = new DatabaseAdapter(context);


        postArrayList = (ArrayList<RssModel>) getIntent().getExtras().get("postList");
        currenPosition = String.valueOf(getIntent().getExtras().get("currentposition"));
        postUrl = getIntent().getExtras().get("currentPostUrl").toString();


        viewPager = (ViewPager) findViewById(viewPagerDetail);


        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        postArrayList = (ArrayList<RssModel>) getIntent().getExtras().get("postList");
        currenPosition = String.valueOf(getIntent().getExtras().get("currentposition"));


        viewPagerDetailAdapter = new ViewPagerDetailAdapter(postArrayList);
        viewPagerDetailAdapter.setOnclickListner(this);

        viewPager.setAdapter(viewPagerDetailAdapter);

        viewPager.setCurrentItem((Integer) getIntent().getExtras().get("currentposition"));

        shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent();

                RssModel rssModel = postArrayList.get(viewPager.getCurrentItem());
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, rssModel.getTitle() + "\n\n" + rssModel.getPostLink() + "\n\nVia " + getString(R.string.app_name) + "\n\n" + getString(R.string.share) + getPackageName() + "\n");
//                +"\n\n\n or \n" +getString(R.string.share)+getPackageName()
                sendIntent.setType("text/plain");
                startActivity(sendIntent);


            }
        });


    }

    public void setArrayList() {

    }

    public void makeActivityFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void initializeId() {
        viewPager = (ViewPager) findViewById(viewPagerDetail);
        shareIcon = (ImageButton) findViewById(R.id.share_fab);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFullScreenWindow();
    }

    public void getFullScreenWindow() {

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    @Override
    public void onClick(View v, int value) {
        postArrayList.get(viewPager.getCurrentItem()).setStar(value);
        viewPagerDetailAdapter.notifyDataSetChanged();
        int count = databaseAdapter.updateStar(postArrayList.get(viewPager.getCurrentItem()).getTitle(), value);
        Toast.makeText(context, count + " rows updated", Toast.LENGTH_SHORT).show();

    }
}
