package mobile.com.hello.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import mobile.com.hello.R;
import mobile.com.hello.util.NetworkUtils;

public class WebsiteActivity extends AppCompatActivity {

    WebView webView;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    Context activityContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        activityContext = this;

        if (toolbar != null) {
            toolbar.setTitle("राजीव दीक्षित जी");
            setSupportActionBar(toolbar);
        }


        String url = getIntent().getStringExtra("URL");
        progressBar = (ProgressBar) findViewById(R.id.progress);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        webView = (WebView) findViewById(R.id.webview);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        this.webView.getSettings().setDisplayZoomControls(false);
        this.webView.getSettings().setDomStorageEnabled(true);
        this.webView.getSettings().setLoadsImagesAutomatically(true);
        this.webView.getSettings().setLoadWithOverviewMode(true);
        this.webView.getSettings().getAllowContentAccess();
        this.webView.setWebViewClient(new WebViewClient());


        //Load website or webpage here
        loadWebPage(url);


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,R.color.colorAccent,R.color.dark_back_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                                    @Override
                                                    public void onRefresh() {
                                                        // This method performs the actual data-refresh operation.
                                                        // The method calls setRefreshing(false) when it's finished.
                                                        //load current webapge here again
                                                        loadWebPage(webView.getUrl());
                                                    }
                                                }
        );


    }

    public void loadWebPage(String url) {
        if (NetworkUtils.isConnected(this)) {
            this.webView.loadUrl(url);
            this.webView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    progressBar.setProgress(progress);

                    if (progress == 100) {
                        progressBar.setProgress(0);
                        swipeRefreshLayout.setRefreshing(false);

                    }

                }
            });
        } else {
            Toast.makeText(this, "No internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }

    }
}
