package mobile.com.hello.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.adapter.DownloadsAdapter;
import mobile.com.hello.models.DownloadsItem;
import mobile.com.hello.util.ReadWriteStorage;

import static android.os.Environment.getExternalStoragePublicDirectory;


public class DownloadsActivity extends AppCompatActivity implements DownloadsAdapter.DownloadsItemClick {

    RecyclerView recyclerView;
    private DownloadsAdapter downloadsAdapter;
    TextView title;
    ImageButton delete;
    Context context, activityContext;
    int position;
    List<DownloadsItem> list;
    boolean firstLaunch = false;


    static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 0;
    View.OnClickListener mOnClickListener;
    private static final int REQUEST_APP_SETTINGS = 168;
    CoordinatorLayout coordinatorLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);

        context = getApplicationContext();
        activityContext = this;

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main);

        title = (TextView) findViewById(R.id.title);
        title.setText("Downloads");

        setupDeleteButton();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //do your work here
            downloadsAdapter = new DownloadsAdapter(getDownloadsData(), this);
            downloadsAdapter.setOnclickListner(this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(downloadsAdapter);

        } else if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(DownloadsActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstLaunch) {
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        }

    }

    @Override
    public void onClick(View v, int position) {

        this.position = position;
        DownloadsItem item = list.get(position);
        String fileName = item.getName();
        if (fileName.contains(".pdf")) {
            openPdfFile(fileName);
        } else if (fileName.contains(".mp3")) {
            playMusic(fileName);
        } else {
            Toast.makeText(this, "Sorry unknown file Found", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //permission granted here

                    downloadsAdapter = new DownloadsAdapter(getDownloadsData(), this);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(downloadsAdapter);


                } else {

                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Storage Permission Required for Downloaded files ", Snackbar.LENGTH_LONG);
//                    TextView text = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_action);
//                    text.setTextSize(14);
//                    text.setMaxLines(3);
                    action.setTextSize(16);
                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setAction("Settings", mOnClickListener);
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                    mOnClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openSettings();

                        }
                    };

                }
            }


        }
    }

    public List<DownloadsItem> getDownloadsData() {
        list = new ArrayList<>();
        DownloadsItem downloadsItem;

        String path = getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/RajivDixitJi";
        File directory = new File(path);
        if (directory.exists()) {

            File[] files = directory.listFiles();
            for (int i = 0; i < files.length; i++) {
                downloadsItem = new DownloadsItem();
                downloadsItem.setName(files[i].getName());
                downloadsItem.setDate(dateFormatter(files[i].getAbsoluteFile().lastModified()));
                downloadsItem.setSize(fileSizeFormatter(files[i].getAbsoluteFile().length()));
                downloadsItem.setBoxSelected(false);
                list.add(downloadsItem);
            }
        }
        return list;
    }

    public String dateFormatter(Long dateValue) {
        String value = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, yyyy HH:mm:ss");
            value = formatter.format(new Date(dateValue));
        } catch (Exception e) {

        }
        return value;

    }

    public String fileSizeFormatter(long length) {
        String fileSize = "";
        if (length < 1024) {
            fileSize = length + "B";
        } else if ((length / 1024) < 1024) {
            fileSize = length / 1024 + " KB";
        } else {
            fileSize = (length / 1024) / 1024 +"." + ((length / 1024) % 1024+"000").substring(0, 2) + " MB";

        }
        return fileSize;
    }

    private void openSettings() {

        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);

    }

    public void setupDeleteButton() {
        delete = (ImageButton) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] fileNames = new String[list.size()];
                int[] positions = new int[list.size()];
                int selections = 0;

                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).isBoxSelected() == true) {
                        fileNames[selections] = list.get(i).getName();
                        selections += 1;
                    }
                }


//use iterator to remove the files from the list with object is recommended
// otherwise with index it will skip some objects

                Iterator<DownloadsItem> itr = list.iterator();
                while (itr.hasNext()) {
                    if (itr.next().isBoxSelected() == true) {
                        itr.remove();
                    }
                }


                if (fileNames[0] == null) {
                    Toast.makeText(context, "Please select the Files", Toast.LENGTH_SHORT).show();
                } else {
                    ReadWriteStorage.deleteDownloadFile(fileNames, getApplicationContext());
                    Toast.makeText(context, selections + " Files Deleted Successfully", Toast.LENGTH_SHORT).show();
                    downloadsAdapter.notifyDataSetChanged();

                }

            }
        });

    }

    public void openPdfFile(String fileName) {
        //boolean for animation on resume
        firstLaunch = true;
        File file = new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/RajivDixitJi/" + fileName);

        try {
            Intent send = new Intent(Intent.ACTION_VIEW);
            send.setDataAndType(Uri.fromFile(file), "application/pdf");
            activityContext.startActivity(send);
            ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

        } catch (android.content.ActivityNotFoundException ex) {
            Intent googleDrive = new Intent(Intent.ACTION_VIEW);
            googleDrive.setDataAndType(Uri.fromFile(file), "application/pdf");
            googleDrive.setPackage("com.google.android.apps.docs");
            startActivity(googleDrive);

            try {
            } catch (Exception e) {
                Toast.makeText(this, "You Don't Have any Pdf Viewer Installed Please install Pdf Viewer", Toast.LENGTH_LONG).show();
            }

        }

    }

    public void playMusic(String filename) {

        activityContext.startActivity(new Intent(DownloadsActivity.this, AudioActivity.class).putExtra("fileName", filename));
        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
    }


}
