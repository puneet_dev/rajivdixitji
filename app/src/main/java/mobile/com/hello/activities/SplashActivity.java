package mobile.com.hello.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import mobile.com.hello.R;
import mobile.com.hello.adapter.DatabaseAdapter;
import mobile.com.hello.model.CategoryModel;
import mobile.com.hello.service.CategoryService;

import static mobile.com.hello.util.Constants.BHARAT_TABLE_NAME;
import static mobile.com.hello.util.Constants.EXPOSE_TABLE_NAME;
import static mobile.com.hello.util.Constants.FARMING_TABLE_NAME;
import static mobile.com.hello.util.Constants.HEALTH_TABLE_NAME;
import static mobile.com.hello.util.Constants.HISTORY_TABLE_NAME;
import static mobile.com.hello.util.Constants.NEWS_TABLE_NAME;
import static mobile.com.hello.util.Constants.RECALL_TABLE_NAME;
import static mobile.com.hello.util.Constants.SWADESHI_TABLE_NAME;


public class SplashActivity extends AppCompatActivity {
    private final Handler handler = new Handler();
    private static int count=0;
    DatabaseAdapter databaseAdapter;
    public static final String MyPREFERENCES = "MyPrefs" ;
    String isDataAvailable;

    private final Runnable startActivityRunnable = new Runnable() {
        public void run() {

            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(intent);
        }
    };

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        databaseAdapter = new DatabaseAdapter(this);

        SharedPreferences sharedPref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        isDataAvailable = sharedPref.getString("db",null);

        if(isDataAvailable!=null && isDataAvailable.equalsIgnoreCase("true")){

            CategoryModel.getInstance().setHealthList(databaseAdapter.getCategoryListFromDb(HEALTH_TABLE_NAME));
            CategoryModel.getInstance().setExposeList(databaseAdapter.getCategoryListFromDb(EXPOSE_TABLE_NAME));
            CategoryModel.getInstance().setHistoryList(databaseAdapter.getCategoryListFromDb(HISTORY_TABLE_NAME));
            CategoryModel.getInstance().setNewsList(databaseAdapter.getCategoryListFromDb(NEWS_TABLE_NAME));
            CategoryModel.getInstance().setRrgList(databaseAdapter.getCategoryListFromDb(RECALL_TABLE_NAME));
            CategoryModel.getInstance().setOrganicFarmingList(databaseAdapter.getCategoryListFromDb(FARMING_TABLE_NAME));
            CategoryModel.getInstance().setIncredibleList(databaseAdapter.getCategoryListFromDb(BHARAT_TABLE_NAME));
            CategoryModel.getInstance().setHomeMadeList(databaseAdapter.getCategoryListFromDb(SWADESHI_TABLE_NAME));
        }else{
            startService(new Intent(this, CategoryService.class));
        }

        this.handler.postDelayed(this.startActivityRunnable, 4000L);

    }

    public void onPause() {
        super.onPause();
        finish();
        this.handler.removeCallbacks(this.startActivityRunnable);
    }

    protected void onResume() {
        super.onResume();
    }
}