package mobile.com.hello.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import mobile.com.hello.R;

/**
 * Created by Honey on 17/Aug/2017.
 */

public class IntroActivity extends AppCompatActivity {

    WebView webView;
    ProgressBar progressBar;
    Toolbar toolbar;
    ImageView backImage;
//    BufferedInputStream inputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        toolbar = (Toolbar) findViewById(R.id.toolbarwebpage_intro);
        backImage = (ImageView) findViewById(R.id.back_button);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String url = getIntent().getStringExtra("URL");
        webView = (WebView) findViewById(R.id.webview);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        this.webView.getSettings().setDisplayZoomControls(false);
        this.webView.getSettings().setDomStorageEnabled(true);
        this.webView.getSettings().setLoadsImagesAutomatically(true);
        this.webView.getSettings().setLoadWithOverviewMode(true);
        this.webView.getSettings().getAllowContentAccess();
        this.webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient());
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }

    }

}
