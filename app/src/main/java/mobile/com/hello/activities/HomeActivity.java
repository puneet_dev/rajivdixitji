package mobile.com.hello.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import mobile.com.hello.CustomViewPager;
import mobile.com.hello.R;
import mobile.com.hello.adapter.NavdarawerAdapter;
import mobile.com.hello.adapter.ViewPagerAdapter;
import mobile.com.hello.core.DataObject;
import mobile.com.hello.dialog.IntroDialog;
import mobile.com.hello.service.CategoryService;
import mobile.com.hello.util.SharedPref;


public class HomeActivity extends AppCompatActivity {
    Context activityContext;
    Context context;
    public Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    RecyclerView navigation_drawer_recycler;
    Bundle bundle;
    public static HomeActivity homeActivity;
    public TabLayout tabLayout;
    public CustomViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    LayoutInflater inflater;

    public String URL;

    CoordinatorLayout coordinatorLayout;
    boolean backPressed = false, firstLaunch = false;
    BroadcastReceiver myReciever;

    int[] navIcons = {R.drawable.intro, R.drawable.latest, R.drawable.website, R.drawable.facebook, R.drawable.youtube, R.drawable.beauty, R.drawable.treatment, R.drawable.health, R.drawable.truth, R.drawable.history, R.drawable.news, R.drawable.recall, R.drawable.bharat, R.drawable.farming, R.drawable.sawdeshi, R.drawable.library, R.drawable.videshi, R.drawable.audio, R.drawable.download_nav, R.drawable.all_stars, R.drawable.sync_now, R.drawable.invite, R.drawable.settings, R.drawable.about_us};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.BlackTheme);
        setContentView(R.layout.activity_home);


        activityContext = this;
        context = getApplicationContext();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        inflater = getLayoutInflater();
        homeActivity = this;
        bundle = new Bundle();
        initView();

        if (toolbar != null) {
            setUpToolBar("राजीव दीक्षित जी", ContextCompat.getColor(context, R.color.lite_text_color), ContextCompat.getColor(context, R.color.colorPrimary));
            setSupportActionBar(toolbar);
        }

        SharedPref sharedPref = new SharedPref(context);
        if (sharedPref.readBoolean("firstLaunchIntro")) {
            new IntroDialog(this, activityContext, inflater, R.layout.dialog_intro, "rajivdixitintro.txt", "राजीव दीक्षित जी का  परिचय").gotoDialog().show();
            sharedPref.writeBoolean("firstLaunchIntro", false);
        }
        initDrawer();

        setUpViewPager();


        myReciever = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                // TODO Auto-generated method stub
                Intent getintent = getIntent();
                String action = getintent.getAction();
                Notification(HomeActivity.this,"Fetched Articles", "Syncing finished....");
//                viewPager.invalidate();
                viewPagerAdapter.getItem(viewPager.getCurrentItem());
                viewPagerAdapter.notifyDataSetChanged();

            }
        };
    }

    public void Notification(Context context, String messageContent, String notificationTitle) {
        // Set Notification Title
        String strtitle = "Syncing completed....";
        // Open NotificationView Class on Notification Click
        /*Intent intent = new Intent(context, NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", message);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);*/

        // Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(
                context)
                // Set Icon
                .setSmallIcon(R.drawable.all_stars)
                // Set Ticker Message
                .setTicker(messageContent)
                // Set Title
                .setContentTitle(notificationTitle)
                // Set Text
                .setContentText(messageContent)
                // Add an Action Button below Notification
//                .addAction(R.drawable.ic_launcher, "Action Button", pIntent)
                // Set PendingIntent into Notification
//                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstLaunch) {
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
        registerBroadcastReceiverDynamically();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        firstLaunch = false;
    }

    @Override
    public void onBackPressed() {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Press Back Once More to Exit", Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.lite_text_color));
        textView.setTextSize(16);
        snackbar.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backPressed = false;
            }
        }, 3000);
        if (backPressed) {
            HomeActivity.this.finish();
            ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

        }
        backPressed = true;
    }

    private void initDrawer() {

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };
        drawerLayout.addDrawerListener(drawerToggle);
    }

    private void setUpToolBar(String title, int titleColor, int backColor) {

        toolbar.setTitle(title);
        toolbar.setTitleTextColor(titleColor);
        toolbar.setBackgroundColor(backColor);
    }

    private void setUpViewPager() {
        viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigation_drawer_recycler = (RecyclerView) findViewById(R.id.navigation_drawer_recycler);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        navigation_drawer_recycler.setLayoutManager(lm);

        ArrayList<DataObject> navlist = new ArrayList<>();
        String[] navarray = getResources().getStringArray(R.array.navi_drawer_items);

        for (int x = 0; x < navarray.length; x++) {
            DataObject navobj = new DataObject();
            navobj.put("title", navarray[x]);
            navlist.add(navobj);
        }
        NavdarawerAdapter navdarawerAdapter = new NavdarawerAdapter(navlist, navIcons);
        navigation_drawer_recycler.setAdapter(navdarawerAdapter);

        ((NavdarawerAdapter) navdarawerAdapter).setOnclickListner(new NavdarawerAdapter.NavigationItemClick() {
            @Override
            public void onClick(View v, int position) {
                firstLaunch = true;
                switch (position) {
                    case 0: {
                        Intent intent = new Intent(HomeActivity.this, IntroActivity.class);
                        intent.putExtra("URL", "file:///android_asset/introduction.html");
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 1: {
                        Intent intent = new Intent(HomeActivity.this, WebsiteActivity.class);
                        intent.putExtra("URL", "http://www.rajivdixitji.com/latest/");
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 2: {
                        Intent intent = new Intent(HomeActivity.this, WebsiteActivity.class);
                        intent.putExtra("URL", "http://www.rajivdixitji.com");
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 3: {
                        activityContext.startActivity(getOpenFacebookIntent(context));
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

                        break;
                    }
                    case 4: {
                        try {
                            //youtubeApp
                            activityContext.startActivity(new Intent(Intent.ACTION_VIEW).setPackage("com.google.android.youtube").setData(Uri.parse("http://www.youtube.com/channel/UCBu1-C7zod8qigd2v1Ea5HA")));
                            ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

                        } catch (Exception ex) {
                            try {
//                                youtubeGoApp
                                activityContext.startActivity(new Intent(Intent.ACTION_VIEW).setPackage("com.google.android.apps.youtube.mango").setData(Uri.parse("http://www.youtube.com/channel/UCBu1-C7zod8qigd2v1Ea5HA")));
                                ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

                            } catch (Exception e) {
                                //WebIntent
                                activityContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/channel/UCBu1-C7zod8qigd2v1Ea5HA")));
                                ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);

                            }
                        }
                        break;
                    }
                    case 5: {
                        Intent intent = new Intent(HomeActivity.this, BeautyActivity.class);
                        intent.putExtra("title", "Natural Beauty Tips");
                        intent.putExtra("drawable", R.drawable.beauty);
                        intent.putExtra("fileName", "beautyindex.txt");
                        intent.putExtra("htmlName", "NBT");
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 6: {
                        Intent intent = new Intent(HomeActivity.this, BeautyActivity.class);
                        intent.putExtra("title", "Ayurvedic Treatment");
                        intent.putExtra("drawable", R.drawable.treatment);
                        intent.putExtra("fileName", "treatmentindex.txt");
                        intent.putExtra("htmlName", "ht");
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 7: {
                        tabLayout.getTabAt(0).select();
                        break;
                    }
                    case 8: {
                        tabLayout.getTabAt(1).select();
                        break;
                    }
                    case 9: {
                        tabLayout.getTabAt(2).select();
                        break;
                    }
                    case 10: {
                        tabLayout.getTabAt(3).select();
                        break;
                    }
                    case 11: {
                        tabLayout.getTabAt(4).select();
                        break;
                    }
                    case 12: {
                        tabLayout.getTabAt(5).select();
                        break;
                    }
                    case 13: {
                        tabLayout.getTabAt(6).select();
                        break;
                    }
                    case 14: {
                        tabLayout.getTabAt(7).select();
                        break;
                    }
                    case 15: {
                        Intent intent = new Intent(HomeActivity.this, BooksActivity.class);
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 16: {
                        Intent intent = new Intent(HomeActivity.this, WebPageStaticActivity.class);
                        intent.putExtra("URL", "file:///android_asset/swadeshi_list.html");
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 17: {
                        Intent intent = new Intent(HomeActivity.this, AudioActivity.class);
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 18: {
                        //star marked
                        Intent intent = new Intent(HomeActivity.this, DownloadsActivity.class);
                        activityContext.startActivity(intent);
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;


                    }
                    case 19: {
                        //star marked
                        activityContext.startActivity(new Intent(HomeActivity.this, StarMarkActivity.class));
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;

                    }
                    case 20: {
                        //sync now
                        int fragmentPosition = viewPager.getCurrentItem();
                        Notification(HomeActivity.this,"Fetching Articles", "Syncing started....");
                        Intent i = new Intent(HomeActivity.this, CategoryService.class);
                        i.putExtra("position", fragmentPosition);
                        startService(i);
//                        viewPagerAdapter.notifyDataSetChanged();




                        break;
                    }
                    case 21: {
                        //Invite Friends
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Download and install " + getString(R.string.app_name) + " App from Playstore with below link \n\n" + getString(R.string.playstore_link) + getPackageName() + "\n");
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        break;
                    }
                    case 22: {
                        // settings
                        activityContext.startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }
                    case 23: {
                        // About Us
                        activityContext.startActivity(new Intent(HomeActivity.this, AboutUsActivity.class));
                        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
                        break;
                    }

                }
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
    }



    public void registerBroadcastReceiverDynamically() {
        IntentFilter PausePlay = new IntentFilter();
        PausePlay.addAction("COMPLETED");
        registerReceiver(myReciever, PausePlay);
    }

    public static Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/160117591008255"));
        } catch (Exception e) {
            try {
                context.getPackageManager().getPackageInfo("com.facebook.lite", 0);
                return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/160117591008255"));
            } catch (PackageManager.NameNotFoundException e1) {

                //open in the browser or open in the webview
                return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/RajivDixitJiOfficial/>"));


            }
        }
    }
}