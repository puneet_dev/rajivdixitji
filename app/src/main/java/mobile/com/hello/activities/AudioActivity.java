package mobile.com.hello.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.adapter.AudioAdapter;
import mobile.com.hello.dialog.AudioDialog;
import mobile.com.hello.dialog.DownloadDialog;
import mobile.com.hello.models.AudioItem;
import mobile.com.hello.util.NetworkUtils;
import mobile.com.hello.util.ReadWriteStorage;
import mobile.com.hello.util.SharedPref;

import static android.os.Environment.getExternalStoragePublicDirectory;


public class AudioActivity extends AppCompatActivity implements AudioAdapter.AudioItemClick {

    RecyclerView recyclerView;
    private AudioAdapter audioAdapter;
    TextView trackName, title, startTime, endTime;
    String downloadFileName, link;
    List<AudioItem> list;
    LayoutInflater inflater;
    int position = 0;


    AudioManager audioManager;
    MediaPlayer player;
    boolean enabled = true;
    public ImageButton playPause, last, next;
    ProgressBar progressBar;
    SeekBar seekBar;
    int audioDuration, currentDuration;
    Handler handler;
    Runnable runnable;
    // is playing is used to check if the player is runnin ot not on launch the player is stoppd so
    //first launch to tell that its very first launch after the app launch
    //is paused for checking if audio is paused before clicking next or previous
    Boolean isPlaying = false, firstLaunch = true, isPaused = false;

    Context appContext, activityContext;
    CoordinatorLayout coordinatorLayout;

    static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 0;
    private static final int REQUEST_APP_SETTINGS = 168;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main);


        activityContext = AudioActivity.this;
        appContext = getApplicationContext();
        inflater = getLayoutInflater();

        title = (TextView) findViewById(R.id.title);
        title.setText("राजीव दीक्षित जी के व्याख्यान");

        //initilize play buttons here
        initializePlayButtons();

        //initilize recycler view and data  here
        initializeRecyclerViewData();

        //initilize mediaplayer class here
        trackName = (TextView) findViewById(R.id.trackname);
        startTime = (TextView) findViewById(R.id.start_time);
        endTime = (TextView) findViewById(R.id.end_time);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 100, 0);

        try {
            //  play audio from downloads
            playAudioOffline(getIntent().getStringExtra("fileName"));
        } catch (Exception e) {

        }
        //for moving the text on the tract title
        trackName.setSelected(true);

        //read shared preferences for first launch
        SharedPref sharedPref = new SharedPref(appContext);
        if (sharedPref.readBoolean("firstLaunchAudio")) {
            new AudioDialog(this, activityContext, inflater, R.layout.dialog_audio, "audiointro.txt", "राजीव दीक्षित जी के  व्याख्यान").gotoDialog().show();
            sharedPref.writeBoolean("firstLaunchAudio", false);
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isPlaying) {
            player.pause();
            player.stop();
            player = null;
        }
        if (player != null) {
            player.pause();
            player.stop();
            player = null;
        }
        if (handler != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        audioPlayer.resumeAudio();
//        playPause.setBackgroundResource(R.drawable.pause);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //permission granted here
                    AudioItem item = list.get(position);
                    downloadFileName = item.getName();
                    new DownloadDialog(this, activityContext, inflater, R.layout.dialog_download, item.getLink(), downloadFileName, ".pdf").gotoDialog().show();


                } else {

                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Storage Permission Required for Audio Downloading", Snackbar.LENGTH_LONG);
//                    TextView text = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_action);
//                    text.setTextSize(14);
//                    text.setMaxLines(3);
                    action.setTextSize(16);
                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setAction("Settings", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openSettings();
                        }
                    });
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ContextCompat.getColor(appContext, R.color.colorPrimary));
                    snackbar.show();

                }
            }


        }
    }

    @Override
    public void onClick(View v, int position) {
        this.position = position;
        AudioItem item = list.get(position);
        downloadFileName = item.getName();
        link = item.getLink();

        if (v.getId() == R.id.download) {

            if (NetworkUtils.isConnected(activityContext)) {
                if (readWritePermission()) {
                    if (downloadedFileFound(ReadWriteStorage.readFileInfo("", ".mp3"))) {
                        Toast.makeText(activityContext, "File Already Downloaded", Toast.LENGTH_LONG).show();
                    } else {
                        downloadAudio(position);
                    }
                }
            } else {
                Toast.makeText(activityContext, "No  Internet Connection", Toast.LENGTH_LONG).show();
            }

        } else if (enabled) { //container is clicked
            if (readWritePermission()) {
                if (downloadedFileFound(ReadWriteStorage.readFileInfo("", ".mp3"))) {
                    playAudioOffline(downloadFileName + ".mp3");
                } else {
                    if (isPaused) {
                        player = null;
                    }
                    playAudioOnline(downloadFileName, link);
                }
            } else {
                //if permissoion not granted
                if (isPaused) {
                    player = null;
                }
                playAudioOnline(downloadFileName, link);

            }

        }
    }

    // this method handles the headset button while activity is in front
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
            //handle click
            if (playPause.isEnabled()) {
                playPause.performClick();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    /////////////////////////////////////////////////////////////////////// METHODS //////////////////////////////////////////////////////////////////////////////////////
    public void initializePlayButtons() {

        progressBar = (ProgressBar) findViewById(R.id.progress);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        seekBar.setEnabled(false);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if ((fromUser && isPlaying) || (fromUser && isPaused)) {
                    startTime.setText(timeFormatter(seekBar.getProgress()));
                    player.seekTo(progress);

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (isPlaying) {
                    player.pause();
                    isPaused = true;
                }
            }


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (isPaused) {
                    resumeAudio();
                    isPaused = false;
                }
            }
        });


        playPause = (ImageButton) findViewById(R.id.play_pause);
        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isPlaying) {
                    //pause audio here
                    pauseAudio();
                } else {
                    //playAudiohere
                    if (firstLaunch) {
                        playAudioOnline("भारत की आजादी का असली इतिहास", "http://rajivdixitji.com/wp-content/uploads/Aajadi_Ka_Asli_Itihaas.mp3");
                        firstLaunch = false;
                    } else {
                        resumeAudio();
                    }

                }

            }
        });
        last = (ImageButton) findViewById(R.id.last);
        last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    position = list.size() - 1;
                } else {
                    position -= 1;
                }
                AudioItem item = list.get(position);
                downloadFileName = item.getName() + ".mp3";
                link = item.getLink();
                if (readWritePermission()) {

                    if (downloadedFileFound(ReadWriteStorage.readFileInfo("", ".mp3"))) {
                        playAudioOffline(downloadFileName);
                    } else {
                        if (isPaused) {
                            player = null;
                        }
                        playAudioOnline(downloadFileName, link);
                    }

                } else {
                    if (isPaused) {
                        player = null;
                    }
                    playAudioOnline(downloadFileName, link);

                }

            }
        });


        next = (ImageButton) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position == (list.size() - 1)) {
                    position = 0;
                } else {
                    position += 1;
                }

                AudioItem item = list.get(position);
                downloadFileName = item.getName() + ".mp3";
                link = item.getLink();
                if (readWritePermission()) {

                    if (downloadedFileFound(ReadWriteStorage.readFileInfo("", ".mp3"))) {
                        playAudioOffline(downloadFileName);
                    } else {

                        if (isPaused) {
                            player = null;
                        }
                        playAudioOnline(downloadFileName, link);
                    }
                } else {
                    if (isPaused) {
                        player = null;
                    }
                    playAudioOnline(downloadFileName, link);
                }
            }
        });


    }

    public void initializeRecyclerViewData() {
        list = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        audioAdapter = new AudioAdapter(getAudioListData(), this);
        audioAdapter.setOnclickListner(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(audioAdapter);
    }

    public List<AudioItem> getAudioListData() {


        try {
//            String jsonLocation = AssetJSONFile("audiolinksfinal.json", this);
            String jsonLocation = readFromFile(this, "audiolinksfinal.json");

            JSONObject jsonobject = new JSONObject(jsonLocation);
            JSONArray jsonArray = jsonobject.getJSONArray("audio");

            AudioItem audioItem;

            for (int i = 0; i < jsonArray.length(); i++) {
                audioItem = new AudioItem();

                JSONObject json = jsonArray.getJSONObject(i);


                audioItem.setLink(json.getString("link"));
                audioItem.setName(json.getString("name"));
                list.add(audioItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    public static String readFromFile(Context context, String file) {
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte buffer[] = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void downloadAudio(int position) {


        if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            //do your work here
            AudioItem item = list.get(position);
            downloadFileName = item.getName();

            new DownloadDialog(this, activityContext, inflater, R.layout.dialog_download, item.getLink(), downloadFileName, ".mp3").gotoDialog().show();


        } else if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

            ActivityCompat.requestPermissions(AudioActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

        }
    }

    private void openSettings() {

        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);

    }

    private void playAudioOnline(String fileName, String link) {
        if (NetworkUtils.isConnected(activityContext)) {
            fileName = modifyFileName(fileName);
            trackName.setText(fileName);
            trackName.setSelected(true);
            playPause.setBackgroundResource(R.drawable.pause);
            toggleButton(false);
            streamAudio(link);

        } else {
            Toast.makeText(activityContext, "No  Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void playAudioOffline(String fileName) {

        String file = getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/RajivDixitJi/" + fileName;

        //to move the track name
        fileName = modifyFileName(fileName);
        trackName.setText(fileName);
        trackName.setSelected(true);
        playPause.setBackgroundResource(R.drawable.pause);
        toggleButton(false);
        loadAudioTrack(file);


    }

    public boolean downloadedFileFound(String[] downloadedFiles) {

        boolean fileFound = false;
        for (int i = 0; i < downloadedFiles.length; i++) {
            if ((downloadFileName + ".mp3").equals(downloadedFiles[i])) {

                fileFound = true;
                break;

            }

        }
        return fileFound;
    }

    public boolean readWritePermission() {
        boolean granted = false;
        if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            granted = true;
        } else {
            granted = false;
        }
        return granted;
    }

    public String modifyFileName(String fileName) {
        if (fileName.contains(".mp3")) {
            fileName = fileName.substring(0, fileName.length() - 4);
        }
        if (fileName.length() < 25) {
            fileName = "                       " + fileName + "                       ";
        } else {
            fileName = "                       " + fileName + "                  ";
        }
        return fileName;
    }


    ////////////// MEDIA PLAYER ////// MEDIAPLAYER //////////////////  MEDIA PLAYER /////////////  MEDIA PLAYER//////////////////////


    // for playing online
    public void streamAudio(String url) {
        firstLaunch = false;

        if (player == null) {
            player = new MediaPlayer();
        } else if (player.isPlaying()) {
            resetPlayer();
        }


        try {
            player.setDataSource(url);
            player.prepareAsync();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    isPlaying = true;
                    isPaused = false;
                    toggleButton(true);
                    audioDuration = player.getDuration();
                    seekBar.setEnabled(true);
                    seekBar.setMax(audioDuration);
                    seekBarUpdate();
                    startTime.setText(timeFormatter(player.getCurrentPosition()));
                    endTime.setText(timeFormatter(audioDuration));

                }
            });
            player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    try {
                        player = null;
                        player = new MediaPlayer();
                    } catch (Exception e) {

                    }

                    return true;
                }
            });


            player.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    Log.d("HHH", percent + "");
                }
            });


        } catch (IOException e) {
        }


    }

    //for playing offline
    public void loadAudioTrack(String address) {
        firstLaunch = false;

        if (player == null) {
            player = new MediaPlayer();
        } else if (isPaused || isPlaying) {
            resetPlayer();
        }

        try {
            player.setDataSource(address);
            player.prepare();
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    isPlaying = true;
                    isPaused = false;
                    toggleButton(true);
                    audioDuration = player.getDuration();
                    seekBar.setEnabled(true);
                    seekBar.setMax(audioDuration);
                    seekBarUpdate();
                    startTime.setText(timeFormatter(player.getCurrentPosition()));
                    endTime.setText(timeFormatter(audioDuration));
                }
            });


        } catch (IOException e) {

        }

    }

    public void resetPlayer() {
        player.pause();
        player.stop();
        player.reset();
        player = null;
        player = new MediaPlayer();
    }

    public void pauseAudio() {
        player.pause();
        isPlaying = false;
        playPause.setBackgroundResource(R.drawable.play);
        isPaused = true;
    }

    public void resumeAudio() {
        player.start();
        playPause.setBackgroundResource(R.drawable.pause);
        isPlaying = true;
        isPaused = false;

    }

    public void toggleButton(boolean value) {
        if (value == true) {
            progressBar.setVisibility(View.INVISIBLE);
            playPause.setVisibility(View.VISIBLE);

        } else {
            playPause.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }
        playPause.setClickable(value);
        next.setClickable(value);
        last.setClickable(value);
        seekBar.setEnabled(false);
        seekBar.setProgress(0);
        enabled = value;
    }

    public void seekBarUpdate() {

        if (runnable == null) {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {

                    if (!isPaused) {
                        int currentPosition = player.getCurrentPosition();
                        seekBar.setProgress(currentPosition);
                        startTime.setText(timeFormatter(currentPosition));
                        if (currentPosition == audioDuration) {
                            resetPlayer();
                            next.performClick();
                        }
                    }

                    handler.postDelayed(this, 1000);
                }
            };
            runnable.run();

        } else {
            runnable = null;
            seekBarUpdate();
        }
    }

    public String timeFormatter(int time) {
        String seconds, minutes;
        int ss = (time / 1000) % 60;
        if (ss < 10) {
            seconds = "0" + ss;
        } else {
            seconds = "" + ss;
        }
        int mm = ((time / 1000) / 60) % 60;
        if (mm < 10) {
            minutes = "0" + mm;
        } else {
            minutes = "" + mm;
        }
        if (time > 3600000) {
            int hh = ((time / 1000) / 60) / 60;
            return hh + ":" + minutes + ":" + seconds;
        } else {
            return minutes + ":" + seconds;
        }

    }


    public void temp() {
        if (player.MEDIA_INFO_BUFFERING_START == MediaPlayer.MEDIA_INFO_BUFFERING_START) {
            Log.d("HH", "buffer ho raha hai ");
        }

    }

}


