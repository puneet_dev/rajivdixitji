package mobile.com.hello.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.adapter.StarMarkAdapter;
import mobile.com.hello.models.StarMarkItem;


public class StarMarkActivity extends AppCompatActivity implements StarMarkAdapter.StarMarkItemClick {

    RecyclerView recyclerView;
    private StarMarkAdapter starMarkAdapter;
    TextView title;
    List<StarMarkItem> list;
    int position;

    Context appContext, activityContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star_mark);

        appContext = getApplicationContext();
        activityContext = StarMarkActivity.this;


        list = new ArrayList<>();

        title = (TextView) findViewById(R.id.title);
        title.setText("\u2606" + "Star Marked" + "\u2606");


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        starMarkAdapter = new StarMarkAdapter(getStarMarkListData(), this);
        starMarkAdapter.setOnclickListner(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(starMarkAdapter);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }


    @Override
    public void onClick(View v, int position) {
        this.position = position;

    }


    public List<StarMarkItem> getStarMarkListData() {

        return list;
    }

}