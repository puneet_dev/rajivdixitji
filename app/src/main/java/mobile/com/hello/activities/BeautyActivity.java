package mobile.com.hello.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.adapter.BeautyAdapter;
import mobile.com.hello.models.BeautyItem;


public class BeautyActivity extends AppCompatActivity implements BeautyAdapter.BeautyItemClick {

    RecyclerView recyclerView;
    private BeautyAdapter beautyAdapter;
    TextView title;
    List<BeautyItem> list;

    int position;
    Context appContext, activityContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beauty);

        appContext = getApplicationContext();
        activityContext = BeautyActivity.this;


        list = new ArrayList<>();

        title = (TextView) findViewById(R.id.title);
        title.setText(getIntent().getStringExtra("title"));


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        beautyAdapter = new BeautyAdapter(getBeautyListData(), this, getIntent().getIntExtra("drawable", 0));
        beautyAdapter.setOnclickListner(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(beautyAdapter);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    public void onClick(View v, int position) {
        this.position = position;
        Intent intent = new Intent(BeautyActivity.this, WebPageStaticActivity.class);
        intent.putExtra("URL", "file:///android_asset/html/" + list.get(position).getLink());
        activityContext.startActivity(intent);
        ((Activity) activityContext).overridePendingTransition(R.anim.start, R.anim.exit);
    }


    public List<BeautyItem> getBeautyListData() {
        try {
            String[] data = readFromFile(this, getIntent().getStringExtra("fileName")).split("[\r\n]+");
            BeautyItem beautyItem;
            String htmlName = getIntent().getStringExtra("htmlName");
            for (int i = 0; i < data.length; i++) {
                beautyItem = new BeautyItem();
                beautyItem.setLink(htmlName + (i + 1) + ".html");
                beautyItem.setName(data[i]);
                list.add(beautyItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static String readFromFile(Context context, String file) {
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte buffer[] = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
