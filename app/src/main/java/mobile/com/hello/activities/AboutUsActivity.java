package mobile.com.hello.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

import mobile.com.hello.R;

public class AboutUsActivity extends AppCompatActivity {

    TextView version;
    Button contactUs;
    boolean firstlaunch = false;
    Toolbar toolbar;
    ImageView backImage;

    private AdView adView;
    LinearLayout adContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        toolbar = (Toolbar) findViewById(R.id.toolbarwebpage_intro);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        //initilize facebook banner ads
        adView = new AdView(this, getResources().getString(R.string.facebook_about_us_banner_id), AdSize.BANNER_HEIGHT_50);
        // Find the Ad Container
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        // Add the ad view to your activity layout
        adContainer.addView(adView);
        // Request an ad
        adView.loadAd();
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                adView.loadAd();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

        });
        backImage = (ImageView) findViewById(R.id.back_button);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        version = (TextView) findViewById(R.id.version);
        version.setText(getVersionName());
        contactUs = (Button) findViewById(R.id.contact_us);
        contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMail();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstlaunch) {
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    public String getVersionName() {
        String version = "V- x.x";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = "V- " + pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public void sendMail() {
        firstlaunch = true;
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "appvriksh@gmail.com", null));
        emailIntent.setPackage("com.google.android.gm");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Rajiv Dixit Ji App");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Name:-\nPhone  No:-\nCity:-\n\nSay something here...");
        try {
            this.startActivity(emailIntent);
            this.overridePendingTransition(R.anim.start, R.anim.exit);

        } catch (android.content.ActivityNotFoundException ex) {
            this.startActivity(Intent.createChooser(emailIntent, "Send email..."));
            this.overridePendingTransition(R.anim.start, R.anim.exit);

        }

    }
}

