package mobile.com.hello.models;


/**
 * Created by puneet on 2/8/17.
 */

public class DownloadsItem {

    String name;
    String date;
    String size;
    boolean boxSelected;

    public boolean isBoxSelected() {
        return boxSelected;
    }

    public void setBoxSelected(boolean selected) {
        this.boxSelected = selected;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
