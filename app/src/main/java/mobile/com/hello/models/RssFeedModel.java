package mobile.com.hello.models;

/**
 * Created by puneet on 25/7/17.
 */

public class RssFeedModel {
    public String title;
    public String link;
    public String description;
    public String content;

    public RssFeedModel(String title, String link, String description, String content) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.content =content;
    }
}
