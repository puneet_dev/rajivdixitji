package mobile.com.hello.models;


/**
 * Created by puneet on 2/8/17.
 */

public class BooksItem {

    String name;
    String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }





}
