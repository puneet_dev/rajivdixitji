package mobile.com.hello.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by puneet on 2/8/17.
 */

public class RssModel implements Parcelable {
    public String title;
//    public String link;
//    public String description;
    public String content;
    public String imagePath;
    public String postLink;
    public int star=0;
    public int uid;




    public String getIsClick() {
        return isClick;
    }

    public void setIsClick(String isClick) {
        this.isClick = isClick;
    }

    public String isClick;

    public RssModel(){

    }

    public RssModel(String title, String postLink, String content, String imagePath, String isClick){
        this.title = title;
        this.content = content;
        this.imagePath = imagePath;
        this.postLink = postLink;
        this.isClick = isClick;
    }

    public RssModel (Parcel parcel) {
        this.title = parcel.readString();
        this.content = parcel.readString();
        this.imagePath = parcel.readString();
        this.postLink = parcel.readString();
        this.isClick = parcel.readString();;
    }


    public String getPostLink() {
        return postLink;
    }

    public void setPostLink(String postLink) {
        this.postLink = postLink;
    }


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /*public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }*/

    /*public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }*/

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public int getUID() {
        return uid;
    }

    public void setUID(int uid) {
        this.uid = uid;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        /*dest.writeString(link);
        dest.writeString(description);*/
        dest.writeString(content);
        dest.writeString(imagePath);
        dest.writeString(postLink);
        dest.writeString(isClick);
    }

    // Method to recreate a Question from a Parcel
    public static Creator<RssModel> CREATOR = new Creator<RssModel>() {

        @Override
        public RssModel createFromParcel(Parcel source) {
            return new RssModel(source);
        }

        @Override
        public RssModel[] newArray(int size) {
            return new RssModel[size];
        }

    };
}
