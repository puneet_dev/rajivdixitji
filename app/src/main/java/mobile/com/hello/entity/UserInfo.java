package mobile.com.hello.entity;

import java.util.List;

import mobile.com.hello.models.RssFeedModel;

/**
 * Created by reflex on 25/7/17.
 */

public class UserInfo {
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    String url="";

    public List<RssFeedModel> getResult() {
        return result;
    }

    public void setResult(List<RssFeedModel> result) {
        this.result = result;
    }

    List<RssFeedModel> result;
}
