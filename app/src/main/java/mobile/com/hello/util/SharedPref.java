package mobile.com.hello.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPref {

    Context appContext;

    public  void writeBoolean(String key, boolean value) {
        SharedPreferences pref = appContext.getSharedPreferences("prefer", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean readBoolean(String key) {
        SharedPreferences pref = appContext.getSharedPreferences("prefer", MODE_PRIVATE);
        return pref.getBoolean(key, true);
    }

    public  void writeInt(String key, int value) {
        SharedPreferences pref = appContext.getSharedPreferences("prefer", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int readInt(String key) {
        SharedPreferences pref = appContext.getSharedPreferences("prefer", MODE_PRIVATE);
        return pref.getInt(key, 0);
    }

    public SharedPref(Context appContext) {
        this.appContext = appContext;
    }


}