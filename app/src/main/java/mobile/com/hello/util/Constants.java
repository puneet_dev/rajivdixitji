package mobile.com.hello.util;

/**
 * Created by reflex on 16/8/17.
 */

public class Constants {

    private static Constants singleton = new Constants();

    private Constants() {

    }

    public static Constants getInstance() {
        return singleton;
    }

    public static String HealthUrl = "http://rajivdixitji.com/category/health/feed/";
    public static String ExposeUrl = "http://rajivdixitji.com/category/expose/feed/";
    public static String HomeUrl = "http://rajivdixitji.com/feed/";
    public static String RealHistoryUrl = "http://rajivdixitji.com/category/history/feed/";
    public static String RightToRecallUrl = "http://rajivdixitji.com/category/rrg/feed/";
    public static String NewsUrl = "http://rajivdixitji.com/category/news/feed/";

    public static String healthString = "Health";
    public static String exposeString = "Expose Truth";
    public static String realTruthString = "Real History";
    public static String swadeshiString = "Swadeshi News";
    public static String rightCallString = "Right to Recall";
    public static String incredibleString = "Incredible India";
    public static String organicString = "Organic Farming";
    public static String productString = "Swadeshi  Product";

    public static final String HEALTH_TABLE_NAME = "health";
    public static final String EXPOSE_TABLE_NAME = "expose";
    public static final String HISTORY_TABLE_NAME = "history";
    public static final String NEWS_TABLE_NAME = "news";
    public static final String RECALL_TABLE_NAME = "recall";
    public static final String BHARAT_TABLE_NAME = "bharat";
    public static final String FARMING_TABLE_NAME = "farming";
    public static final String SWADESHI_TABLE_NAME = "swadeshi";

}
