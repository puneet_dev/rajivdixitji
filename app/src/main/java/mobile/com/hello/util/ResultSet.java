package mobile.com.hello.util;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.models.RssModel;

/**
 * Created by puneet on 25/7/17.
 */

public class ResultSet {
    public List<RssFeedModel> getResult() {
        return result;
    }

    public void setResult(List<RssFeedModel> result) {
        this.result = result;
    }

    List<RssFeedModel> result;


    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    ArrayList<String> image;

    public ArrayList<RssModel> getRssModelArrayList() {
        return rssModelArrayList;
    }

    public void setRssModelArrayList(ArrayList<RssModel> rssModelArrayList) {
        this.rssModelArrayList = rssModelArrayList;
    }

    ArrayList<RssModel> rssModelArrayList;
}
