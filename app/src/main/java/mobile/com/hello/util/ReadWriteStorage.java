package mobile.com.hello.util;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

import static android.os.Environment.getExternalStoragePublicDirectory;


public class ReadWriteStorage {

    public static String[] readFileInfo(String address, String extension) {

        String[] list = {};
        String path = getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/RajivDixitJi/" + address;
        File directory = new File(path);
        if (directory.exists()) {

            File[] files = directory.listFiles();
            list = new String[files.length];
            for (int i = 0; i < files.length; i++) {

                if (files[i].getName().toString().contains(extension)) {
                    list[i] = files[i].getName().toString();
                }
            }

        }

        return list;

    }

    public static void deleteDownloadFile(String[] path, Context context) {

        String address = getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/RajivDixitJi/";

        for (int i = 0; i < path.length; i++) {
            File file = new File(address + path[i]);
            file.delete();
            if (file.exists()) {
                try {
                    file.getCanonicalFile().delete();
                } catch (IOException e) {

                }
                if (file.exists()) {
                    context.deleteFile(file.getAbsolutePath());
                }
            }
        }
    }

}