package mobile.com.hello.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.FeedAsync;
import mobile.com.hello.adapter.DatabaseAdapter;
import mobile.com.hello.asynctask.ConvertAsyncTask;
import mobile.com.hello.entity.UserInfo;
import mobile.com.hello.interfaces.IAsyncTask;
import mobile.com.hello.model.CategoryModel;
import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.models.RssModel;
import mobile.com.hello.util.Util;

import static mobile.com.hello.util.Constants.BHARAT_TABLE_NAME;
import static mobile.com.hello.util.Constants.EXPOSE_TABLE_NAME;
import static mobile.com.hello.util.Constants.FARMING_TABLE_NAME;
import static mobile.com.hello.util.Constants.HEALTH_TABLE_NAME;
import static mobile.com.hello.util.Constants.HISTORY_TABLE_NAME;
import static mobile.com.hello.util.Constants.NEWS_TABLE_NAME;
import static mobile.com.hello.util.Constants.RECALL_TABLE_NAME;
import static mobile.com.hello.util.Constants.SWADESHI_TABLE_NAME;

/**
 * Created by reflex on 1/10/17.
 */

public class CategoryIndividualService extends Service implements IAsyncTask {
    private static int count = 0;
    DatabaseAdapter databaseAdapter;
    public static final String SERVICEPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;
    String position;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        count = 0;
        databaseAdapter = new DatabaseAdapter(this);
        sharedpreferences = getSharedPreferences(SERVICEPREFERENCES, Context.MODE_PRIVATE);

        if (!intent.getExtras().get("position").toString().isEmpty()) {
            position = intent.getExtras().get("position").toString();
            if (position.equalsIgnoreCase("0")) {
                if (Util.haveNetworkConnection(getApplicationContext())) {
                    parseFeed("http://rajivdixitji.com/category/health/feed/");
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    stopSelf();
                }
            }
        }

        /*if(Util.haveNetworkConnection(getApplicationContext())){
            parseFeed("http://rajivdixitji.com/category/health/feed/");
        }else{
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            stopSelf();
        }*/
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public Fragment getFragment() {
        return null;
    }

    @Override
    public void OnPreExecute() {

    }

    @Override
    public void OnPostExecute(String url, List<RssFeedModel> result) {
        if (result != null) {
            UserInfo userInfo = new UserInfo();
            userInfo.setResult(result);
            ConvertAsyncTask convertAsyncTask = new ConvertAsyncTask(userInfo, this);
            convertAsyncTask.execute();
        } else {
            Toast.makeText(this, "No Post available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void OnPostExecute(ArrayList<String> result) {

    }

    @Override
    public void OnPostExecute(String message, ArrayList<RssModel> result) {
        if (result != null && result.size() > 0) {
            if (position.equalsIgnoreCase("0")) {
                CategoryModel.getInstance().setHealthList(result);
                if (databaseAdapter.getRecordsCount(HEALTH_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(HEALTH_TABLE_NAME);
                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", HEALTH_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                sendBroadcastIntent();
            } else if (position.equalsIgnoreCase("1")) {
                CategoryModel.getInstance().setExposeList(result);

                if (databaseAdapter.getRecordsCount(EXPOSE_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(EXPOSE_TABLE_NAME);

                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", EXPOSE_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (position.equalsIgnoreCase("2")) {
                CategoryModel.getInstance().setHistoryList(result);

                if (databaseAdapter.getRecordsCount(HISTORY_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(HISTORY_TABLE_NAME);

                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", HISTORY_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (position.equalsIgnoreCase("3")) {
                CategoryModel.getInstance().setNewsList(result);

                if (databaseAdapter.getRecordsCount(NEWS_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(NEWS_TABLE_NAME);

                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", NEWS_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (position.equalsIgnoreCase("4")) {
                CategoryModel.getInstance().setRrgList(result);

                if (databaseAdapter.getRecordsCount(RECALL_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(RECALL_TABLE_NAME);

                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", RECALL_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (position.equalsIgnoreCase("5")) {
                CategoryModel.getInstance().setIncredibleList(result);

                if (databaseAdapter.getRecordsCount(BHARAT_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(BHARAT_TABLE_NAME);

                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", BHARAT_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (position.equalsIgnoreCase("6")) {
                CategoryModel.getInstance().setOrganicFarmingList(result);

                if (databaseAdapter.getRecordsCount(FARMING_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(FARMING_TABLE_NAME);

                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", FARMING_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (position.equalsIgnoreCase("7")) {
                CategoryModel.getInstance().setHomeMadeList(result);

                if (databaseAdapter.getRecordsCount(SWADESHI_TABLE_NAME) > 0)
                    databaseAdapter.removeAllRecords(SWADESHI_TABLE_NAME);

                try {
                    for (int i = 0; i < result.size(); i++) {
                        //databaseAdapter.dataInsert(String.valueOf(i), result.get(i).getTitle(), result.get(i).getContent(), result.get(i).getPostLink(), "30 sep", result.get(i).getImagePath(), "1", "1", SWADESHI_TABLE_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /*if(count==0){

                CategoryModel.getInstance().setHealthList(result);

                count =1;

                if(databaseAdapter.getRecordsCount(HEALTH_TABLE_NAME)>0)
                databaseAdapter.removeAllRecords(HEALTH_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", HEALTH_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

                parseFeed("http://rajivdixitji.com/category/expose/feed/");

            }
            else if(count ==1){
                count =2;
                CategoryModel.getInstance().setExposeList(result);

                if(databaseAdapter.getRecordsCount(EXPOSE_TABLE_NAME)>0)
                    databaseAdapter.removeAllRecords(EXPOSE_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", EXPOSE_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


                parseFeed("http://rajivdixitji.com/category/history/feed/");

            }else if(count ==2){
                count=3;
                CategoryModel.getInstance().setHistoryList(result);

                if(databaseAdapter.getRecordsCount(HISTORY_TABLE_NAME)>0)
                    databaseAdapter.removeAllRecords(HISTORY_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", HISTORY_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


                parseFeed("http://rajivdixitji.com/category/news/feed/");

            }else if(count ==3){
                count=4;
                CategoryModel.getInstance().setNewsList(result);

                if(databaseAdapter.getRecordsCount(NEWS_TABLE_NAME)>0)
                    databaseAdapter.removeAllRecords(NEWS_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", NEWS_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


                parseFeed("http://rajivdixitji.com/category/rrg/feed/");

            }else if(count ==4){
                count=5;
                CategoryModel.getInstance().setRrgList(result);

                if(databaseAdapter.getRecordsCount(RECALL_TABLE_NAME)>0)
                    databaseAdapter.removeAllRecords(RECALL_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", RECALL_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


                parseFeed("http://rajivdixitji.com/category/incredible-india/feed/");

            }else if(count==5){
                count=6;
                CategoryModel.getInstance().setIncredibleList(result);

                if(databaseAdapter.getRecordsCount(BHARAT_TABLE_NAME)>0)
                    databaseAdapter.removeAllRecords(BHARAT_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", BHARAT_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


                parseFeed("http://rajivdixitji.com/category/organic-farming/feed/");

            }else if(count ==6){
                count=7;
                CategoryModel.getInstance().setOrganicFarmingList(result);

                if(databaseAdapter.getRecordsCount(FARMING_TABLE_NAME)>0)
                    databaseAdapter.removeAllRecords(FARMING_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", FARMING_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


                parseFeed("http://rajivdixitji.com/category/homemade/feed/");

            }else if(count ==7) {
                CategoryModel.getInstance().setHomeMadeList(result);

                if(databaseAdapter.getRecordsCount(SWADESHI_TABLE_NAME)>0)
                    databaseAdapter.removeAllRecords(SWADESHI_TABLE_NAME);

                try{
                    for(int i=0; i<result.size();i++){
                        databaseAdapter.dataInsert(String.valueOf(i),result.get(i).getTitle(), result.get(i).getContent(),result.get(i).getPostLink(), "30 sep",result.get(i).getImagePath(), "1","1", SWADESHI_TABLE_NAME);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                SharedPreferences.Editor editor = sharedpreferences.edit();

                editor.putString("db", "true");
                editor.commit();
                sendBroadcastIntent();

                Toast.makeText(this, "Data Sync done", Toast.LENGTH_SHORT).show();
                stopSelf();
            }
        } else {
            Toast.makeText(this, "No Data Available", Toast.LENGTH_SHORT).show();
            stopSelf();
        }*/
        }
    }

    public void sendBroadcastIntent() {
        Intent intent = new Intent();
        intent.setAction("COMPLETED");
        sendBroadcast(intent);
    }

    @Override
    public void OnErrorMessage(String message) {

    }

    private void parseFeed(String url) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUrl(url);
        FeedAsync feedAsync = new FeedAsync(userInfo, this);
        feedAsync.execute();
    }
}
