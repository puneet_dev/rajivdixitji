package mobile.com.hello.adapter;

import android.app.LauncherActivity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.models.DownloadsItem;

public class DownloadsAdapter extends RecyclerView.Adapter<DownloadsAdapter.DownloadsHolder> {

    List<DownloadsItem> downloadListData;
    private LayoutInflater inflater;
    Context context;
    DownloadsItemClick downloadsItemClick;


    public DownloadsAdapter(List<DownloadsItem> listData, Context context) {
        this.inflater = LayoutInflater.from(context);
        this.downloadListData = listData;
        this.context = context;

    }


    @Override
    public DownloadsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_row_downloads, parent, false);
        return new DownloadsHolder(view);
    }


    @Override
    public void onBindViewHolder(DownloadsHolder holder, int position) {

        DownloadsItem item = downloadListData.get(position);
        holder.name.setText(item.getName());
        holder.size.setText(item.getSize());
        holder.date.setText(item.getDate());
        if (item.getName().contains(".mp3")) {
            holder.icon.setImageResource(R.drawable.mp3);
        } else {
            holder.icon.setImageResource(R.drawable.pdf);
        }
        if (item.isBoxSelected() == false) {
            holder.checkBox.setChecked(false);
        } else {
            holder.checkBox.setChecked(true);

        }



    }

    @Override
    public int getItemCount() {
        return downloadListData.size();
    }

    class DownloadsHolder extends RecyclerView.ViewHolder {
        TextView name, size, date;
        ImageView icon;
        RelativeLayout container;
        CheckBox checkBox;


        public DownloadsHolder(final View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            name = (TextView) itemView.findViewById(R.id.name);
            size = (TextView) itemView.findViewById(R.id.size);
            date = (TextView) itemView.findViewById(R.id.date);
            checkBox = (CheckBox) itemView.findViewById(R.id.check_box);
            container = (RelativeLayout) itemView.findViewById(R.id.container);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (downloadsItemClick != null) {
                        downloadsItemClick.onClick(itemView, getAdapterPosition());
                    }
                }
            });

            checkBox = (CheckBox) itemView.findViewById(R.id.check_box);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (downloadsItemClick != null) {
                        if(downloadListData.get(getAdapterPosition()).isBoxSelected()==false){
                            checkBox.setChecked(true);
                            downloadListData.get(getAdapterPosition()).setBoxSelected(true);
                        }else{
                            checkBox.setChecked(false);
                            downloadListData.get(getAdapterPosition()).setBoxSelected(false);
                        }

                    }
                }
            });
        }
    }

    public void setOnclickListner(DownloadsAdapter.DownloadsItemClick downloadsItemClick) {
        this.downloadsItemClick = downloadsItemClick;
    }

    public interface DownloadsItemClick {
        void onClick(View v, int position);
    }
}


