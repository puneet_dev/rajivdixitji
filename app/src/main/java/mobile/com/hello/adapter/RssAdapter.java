package mobile.com.hello.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import java.util.ArrayList;

import mobile.com.hello.R;
import mobile.com.hello.activities.DetailActivity;
import mobile.com.hello.models.RssModel;

/**
 * Created by puneet on 2/8/17.
 */

public class RssAdapter extends RecyclerView.Adapter<RssAdapter.ViewHolder> {
    private Context context;
    ArrayList<RssModel> postList;
    String category;
    int lastPosition = -1;

    public RssAdapter(Context context, ArrayList<RssModel> postList, String category) {
        this.context = context;
        this.postList = postList;
        this.category = category;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rss_row, parent, false);
        return new RssAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d("Binder",""+position);
        if (postList != null) {
            try {
                String postTitleString = postList.get(position).getTitle();
                String postImage = postList.get(position).getImagePath();
//                String htmlText = " %s ";
//                holder.postTitle.loadData(String.format(htmlText, postTitleString), "text/html", "utf-8");
                if (!postTitleString.equalsIgnoreCase("")) {
                    holder.postTitle.setText(postTitleString);
                }
                if (postImage != null && !postImage.equalsIgnoreCase("")) {
                    Picasso.with(context)
                            .load(postImage)
                            .error(R.drawable.not_avail)
                            .placeholder(R.drawable.not_avail)
                            .into(holder.postImage);
                } else {
                    Picasso.with(context)
                            .load(R.drawable.not_avail)
                            .error(R.drawable.not_avail)
                            .into(holder.postImage);
                }

//                holder.postCategory.setPaintFlags(holder.postCategory.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//                holder.postCategory.setText(category);


                holder.postCardViewLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, DetailActivity.class);
                        intent.putParcelableArrayListExtra("postList", postList);
                        intent.putExtra("currentposition", position);
                        intent.putExtra("currentPostUrl", postList.get(position).getPostLink());
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.start, R.anim.exit);

                    }
                });

                holder.postTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, DetailActivity.class);
                        intent.putParcelableArrayListExtra("postList", postList);
                        intent.putExtra("currentposition", position);
                        intent.putExtra("currentPostUrl", postList.get(position).getPostLink());
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.start, R.anim.exit);

                    }
                });

//                if (position > lastPosition) {
//                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
//                    holder.itemView.startAnimation(animation);
//                    lastPosition = position;
//                } else if (position < lastPosition) {
//                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.down_from_top);
//                    holder.itemView.startAnimation(animation);
//                    lastPosition = position;
//
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView postImage;
        TextView postCategory;
        JustifiedTextView postTitle;
        CardView postCardViewLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            postImage = (ImageView) itemView.findViewById(R.id.post_image);
            postTitle = (JustifiedTextView) itemView.findViewById(R.id.post_title);
            postCardViewLayout = (CardView) itemView.findViewById(R.id.post_card);
//            postCategory = (TextView) itemView.findViewById(R.id.category_text);
        }
    }


}
