package mobile.com.hello.adapter;

import android.app.LauncherActivity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.models.AudioItem;
import mobile.com.hello.models.BooksItem;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.BooksHolder> {

    List<BooksItem> booksListData;
    private LayoutInflater inflater;
    Context context;

    BooksItemClick booksItemClick;


    public BooksAdapter(List<BooksItem> listData, Context context) {
        this.inflater = LayoutInflater.from(context);
        this.booksListData = listData;
        this.context = context;

    }


    @Override
    public BooksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_row_books, parent, false);
        return new BooksHolder(view);
    }


    @Override
    public void onBindViewHolder(BooksHolder holder, int position) {

        BooksItem item = booksListData.get(position);
        holder.name.setText(item.getName());
        holder.index.setText(position + 1 + ".");
    }

    @Override
    public int getItemCount() {
        return booksListData.size();
    }

    class BooksHolder extends RecyclerView.ViewHolder {
        TextView name, index;
        RelativeLayout container;

        public BooksHolder(final View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            index = (TextView) itemView.findViewById(R.id.index);

            container = (RelativeLayout) itemView.findViewById(R.id.root_cont);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (booksItemClick != null)
                        booksItemClick.onClick(itemView, getAdapterPosition());
                }
            });

        }
    }

    public void setOnclickListner(BooksItemClick booksItemClick) {
        this.booksItemClick = booksItemClick;
    }

    public interface BooksItemClick {
        void onClick(View v, int position);
    }


}
