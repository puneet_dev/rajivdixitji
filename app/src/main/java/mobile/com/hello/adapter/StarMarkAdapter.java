package mobile.com.hello.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.models.StarMarkItem;

public class StarMarkAdapter extends RecyclerView.Adapter<StarMarkAdapter.StarMarkHolder> {

    List<StarMarkItem> StarMarkListData;
    private LayoutInflater inflater;
    Context context;

    StarMarkItemClick StarMarkItemClick;


    public StarMarkAdapter(List<StarMarkItem> listData, Context context) {
        this.inflater = LayoutInflater.from(context);
        this.StarMarkListData = listData;
        this.context = context;

    }

    @Override
    public StarMarkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_row_star_mark, parent, false);
        return new StarMarkHolder(view);
    }

    @Override
    public void onBindViewHolder(StarMarkHolder holder, int position) {

        StarMarkItem item = StarMarkListData.get(position);
        holder.postTitle.setText(item.getTitle());
        holder.postImage.setImageResource(R.drawable.not_avail);
    }

    @Override
    public int getItemCount() {
        return StarMarkListData.size();
    }

    class StarMarkHolder extends RecyclerView.ViewHolder {
        TextView postTitle;
        CardView cardView;
        ImageView postImage;

        public StarMarkHolder(final View itemView) {
            super(itemView);
            postTitle = (TextView) itemView.findViewById(R.id.post_title);
            postImage = (ImageView) itemView.findViewById(R.id.post_image);
            cardView = (CardView) itemView.findViewById(R.id.post_card);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (StarMarkItemClick != null) {
                        StarMarkItemClick.onClick(v, getAdapterPosition());
                    }
                }
            });
        }
    }


    public void setOnclickListner(StarMarkItemClick StarMarkItemClick) {
        this.StarMarkItemClick = StarMarkItemClick;
    }

    public interface StarMarkItemClick {
        void onClick(View v, int position);
    }
}
