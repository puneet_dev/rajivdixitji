package mobile.com.hello.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.models.AudioItem;

public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.AudioHolder> {

    List<AudioItem> audioListData;
    private LayoutInflater inflater;
    Context context;

    AudioItemClick audioItemClick;


    public AudioAdapter(List<AudioItem> listData, Context context) {
        this.inflater = LayoutInflater.from(context);
        this.audioListData = listData;
        this.context = context;

    }


    @Override
    public AudioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_row_audio, parent, false);
        return new AudioHolder(view);
    }


    @Override
    public void onBindViewHolder(AudioHolder holder, int position) {

        AudioItem item = audioListData.get(position);
        holder.name.setText(item.getName());
        holder.index.setText(position + 1 + ".");
    }

    @Override
    public int getItemCount() {
        return audioListData.size();
    }


    class AudioHolder extends RecyclerView.ViewHolder {
        TextView name, index;
        RelativeLayout container;
        ImageButton downloads;


        public AudioHolder(final View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            index = (TextView) itemView.findViewById(R.id.index);
            downloads = (ImageButton) itemView.findViewById(R.id.download);
            container = (RelativeLayout) itemView.findViewById(R.id.root_cont);
            downloads.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (audioItemClick != null) {

                        audioItemClick.onClick(v, getAdapterPosition());


                    }
                }
            });
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (audioItemClick != null) {

                        audioItemClick.onClick(v, getAdapterPosition());


                    }
                }
            });
        }
    }


    public void setOnclickListner(AudioItemClick audioItemClick) {
        this.audioItemClick = audioItemClick;
    }

    public interface AudioItemClick {
        void onClick(View v, int position);
    }
}
