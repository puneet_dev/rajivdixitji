package mobile.com.hello.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.models.DataItem;
import mobile.com.hello.models.RssModel;

/**
 * Created by Honey on 18/Aug/2017.
 */

public class DatabaseAdapter {
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;


    public DatabaseAdapter(Context context) {
        dbHelper = new DbHelper(context);
        sqLiteDatabase = dbHelper.getWritableDatabase();

    }

    public long dataInsert(String uid, String title, String description, String postlink, String date, String imagelink,  String tableName) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(dbHelper.TITLE, title);
        contentValues.put(dbHelper.DESCRIPTION, description);
        contentValues.put(dbHelper.POSTLINK, postlink);
        contentValues.put(dbHelper.DATE, date);
        contentValues.put(dbHelper.IMAGELINK, imagelink);
//        contentValues.put(dbHelper.STAR, star);
//        contentValues.put(dbHelper.READ, read);

        long id = db.insert(tableName, null, contentValues);
        return id;
    }


    public List<DataItem> dataRead() {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {dbHelper.UID, dbHelper.TITLE, dbHelper.DESCRIPTION, dbHelper.POSTLINK, dbHelper.DATE, dbHelper.IMAGELINK, dbHelper.STAR, dbHelper.READ};
        Cursor cursor = db.query(dbHelper.HEALTH_TABLE_NAME, columns, null, null, null, null, null);

        List<DataItem> data = new ArrayList<>();
        DataItem dataItem;
        while (cursor.moveToNext()) {

//            int index = cursor.getColumnIndex(dbHelper.UID);
//            int cid = cursor.getInt(0);
            String uid = cursor.getString(0);
            String title = cursor.getString(1);
            String description = cursor.getString(2);
            String postlink = cursor.getString(3);
            String date = cursor.getString(4);
            String imagelink = cursor.getString(5);
            String star = cursor.getString(6);
            String read = cursor.getString(7);

            dataItem = new DataItem();
            dataItem.setUid(uid);
            dataItem.setTitle(title);
            dataItem.setDescription(description);
            dataItem.setPostlink(postlink);
            dataItem.setDate(date);
            dataItem.setImagelink(imagelink);
            dataItem.setStar(star);
            dataItem.setRead(read);
            data.add(dataItem);

        }
        return data;
    }

    public int getRecordsCount(String tableName) {
        String countQuery = "SELECT  * FROM " + tableName;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public void removeAllRecords(String tableName)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.execSQL("delete from "+ tableName);
//        db.delete(dbHelper.HEALTH_TABLE_NAME, null, null);
    }

    public ArrayList<RssModel> getCategoryListFromDb(String tableName){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {dbHelper.UID, dbHelper.TITLE, dbHelper.DESCRIPTION, dbHelper.POSTLINK, dbHelper.DATE, dbHelper.IMAGELINK, dbHelper.STAR, dbHelper.READ};
        Cursor cursor = db.query(tableName, columns, null, null, null, null, null);
        ArrayList<RssModel> rssArraylist = new ArrayList<>();
        RssModel rssModel;

        while (cursor.moveToNext()) {
            int uid = cursor.getInt(0);
            String title = cursor.getString(1);
            String description = cursor.getString(2);
            String postlink = cursor.getString(3);
            String date = cursor.getString(4);
            String imagelink = cursor.getString(5);
            int star = cursor.getInt(6);
            String read = cursor.getString(7);

            rssModel = new RssModel();
            rssModel.setUID(uid);
            rssModel.setTitle(title);
            rssModel.setPostLink(postlink);
            rssModel.setContent(description);
            rssModel.setImagePath(imagelink);
            rssModel.setStar(star);

            rssArraylist.add(rssModel);
        }

        return rssArraylist;

    }

    public int updateStar(String title, int  newValue) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(dbHelper.STAR, newValue);
        String[] whereArgs = {title};
        int count = db.update(dbHelper.HEALTH_TABLE_NAME, contentValues, dbHelper.TITLE + " =? ", whereArgs);
        return count;
    }

    public int deleteName(String name) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] whereArgs = {name};
        int count = db.delete(dbHelper.HEALTH_TABLE_NAME, dbHelper.UID + " =? ", whereArgs);
        return count;
    }

    public class DbHelper extends SQLiteOpenHelper {
        Context context;
        private static final String DATABASE_NAME = "rajivdixitji";

        private static final String HEALTH_TABLE_NAME = "health";
        private static final String EXPOSE_TABLE_NAME = "expose";
        private static final String HISTORY_TABLE_NAME = "history";
        private static final String NEWS_TABLE_NAME = "news";
        private static final String RECALL_TABLE_NAME = "recall";
        private static final String BHARAT_TABLE_NAME = "bharat";
        private static final String FARMING_TABLE_NAME = "farming";
        private static final String SWADESHI_TABLE_NAME = "swadeshi";

        private static final int DATBASE_VERSION = 1;

        private static final String UID = "_id";
        private static final String TITLE = "title";
        private static final String DESCRIPTION = "description";
        private static final String POSTLINK = "post";
        private static final String DATE = "name";
        private static final String IMAGELINK = "imagelink";
        private static final String STAR = "star";
        private static final String READ = "read";

        private static final String CREATE_TABLE_HEALTH = "CREATE TABLE " + HEALTH_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";
        private static final String CREATE_TABLE_EXPOSE = "CREATE TABLE " + EXPOSE_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";
        private static final String CREATE_TABLE_HISTORY = "CREATE TABLE " + HISTORY_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";
        private static final String CREATE_TABLE_NEWS = "CREATE TABLE " + NEWS_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";
        private static final String CREATE_TABLE_RECALL = "CREATE TABLE " + RECALL_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";
        private static final String CREATE_TABLE_BHARAT = "CREATE TABLE " + BHARAT_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";
        private static final String CREATE_TABLE_FARMING = "CREATE TABLE " + FARMING_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";
        private static final String CREATE_TABLE_SWADESHI = "CREATE TABLE " + SWADESHI_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + POSTLINK + " TEXT, " + DATE + " TEXT, " + IMAGELINK + " TEXT, " + STAR + " INTEGER DEFAULT 0, " + READ + " INTEGER DEFAULT 0 );";

        private static final String DROP_TABLE1 = "DROP TABLE  IF EXISTS " + HEALTH_TABLE_NAME;
        private static final String DROP_TABLE2 = "DROP TABLE  IF EXISTS " + EXPOSE_TABLE_NAME;
        private static final String DROP_TABLE3 = "DROP TABLE  IF EXISTS " + HISTORY_TABLE_NAME;
        private static final String DROP_TABLE4 = "DROP TABLE  IF EXISTS " + NEWS_TABLE_NAME;
        private static final String DROP_TABLE5 = "DROP TABLE  IF EXISTS " + RECALL_TABLE_NAME;
        private static final String DROP_TABLE6 = "DROP TABLE  IF EXISTS " + BHARAT_TABLE_NAME;
        private static final String DROP_TABLE7 = "DROP TABLE  IF EXISTS " + FARMING_TABLE_NAME;
        private static final String DROP_TABLE8 = "DROP TABLE  IF EXISTS " + SWADESHI_TABLE_NAME;

        public DbHelper(Context context) {

            super(context, DATABASE_NAME, null, DATBASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            try {
                sqLiteDatabase.execSQL(CREATE_TABLE_HEALTH);
                sqLiteDatabase.execSQL(CREATE_TABLE_EXPOSE);
                sqLiteDatabase.execSQL(CREATE_TABLE_HISTORY);
                sqLiteDatabase.execSQL(CREATE_TABLE_NEWS);
                sqLiteDatabase.execSQL(CREATE_TABLE_RECALL);
                sqLiteDatabase.execSQL(CREATE_TABLE_BHARAT);
                sqLiteDatabase.execSQL(CREATE_TABLE_FARMING);
                sqLiteDatabase.execSQL(CREATE_TABLE_SWADESHI);

                Toast.makeText(context, "oncreate table ", Toast.LENGTH_LONG).show();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

            sqLiteDatabase.execSQL(DROP_TABLE1);
            sqLiteDatabase.execSQL(DROP_TABLE2);
            sqLiteDatabase.execSQL(DROP_TABLE3);
            sqLiteDatabase.execSQL(DROP_TABLE4);
            sqLiteDatabase.execSQL(DROP_TABLE5);
            sqLiteDatabase.execSQL(DROP_TABLE6);
            sqLiteDatabase.execSQL(DROP_TABLE7);
            sqLiteDatabase.execSQL(DROP_TABLE8);
            onCreate(sqLiteDatabase);
            Toast.makeText(context, "onUpgrade table ", Toast.LENGTH_LONG).show();


        }
    }
}