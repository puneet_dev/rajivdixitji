package mobile.com.hello.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import mobile.com.hello.R;
import mobile.com.hello.models.BeautyItem;

public class BeautyAdapter extends RecyclerView.Adapter<BeautyAdapter.BeautyHolder> {

    List<BeautyItem> beautyListData;
    private LayoutInflater inflater;
    Context context;
    int drawable;

    BeautyItemClick beautyItemClick;


    public BeautyAdapter(List<BeautyItem> listData, Context context, int drawable) {
        this.inflater = LayoutInflater.from(context);
        this.beautyListData = listData;
        this.context = context;
        this.drawable = drawable;
    }


    @Override
    public BeautyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_row_books, parent, false);
        return new BeautyHolder(view);
    }


    @Override
    public void onBindViewHolder(BeautyHolder holder, int position) {

        BeautyItem item = beautyListData.get(position);
        holder.name.setText(item.getName());
        holder.index.setText(position + 1 + ".");
        holder.download.setImageResource(drawable);
    }

    @Override
    public int getItemCount() {
        return beautyListData.size();
    }

    class BeautyHolder extends RecyclerView.ViewHolder {
        TextView name, index;
        RelativeLayout container;
        ImageView download;

        public BeautyHolder(final View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            index = (TextView) itemView.findViewById(R.id.index);
            download = (ImageView) itemView.findViewById(R.id.download);

            container = (RelativeLayout) itemView.findViewById(R.id.root_cont);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (beautyItemClick != null)
                        beautyItemClick.onClick(itemView, getAdapterPosition());
                }
            });

        }
    }

    public void setOnclickListner(BeautyItemClick beautyItemClick) {
        this.beautyItemClick = beautyItemClick;
    }

    public interface BeautyItemClick {
        void onClick(View v, int position);
    }


}
