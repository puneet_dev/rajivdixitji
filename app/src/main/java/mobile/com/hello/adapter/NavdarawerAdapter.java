package mobile.com.hello.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mobile.com.hello.R;
import mobile.com.hello.core.DataObject;


public class NavdarawerAdapter extends RecyclerView.Adapter<NavdarawerAdapter.ViewHolder> {
    ArrayList<DataObject> navlist = new ArrayList<>();
    int[] navIcons ={};

    NavigationItemClick navigationItemClick;

    public NavdarawerAdapter(ArrayList<DataObject> navlist, int[] navIcons) {
        this.navlist = navlist;
        this.navIcons=navIcons;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DataObject navObj = navlist.get(position);
        String navtxt = (String) navObj.get("title");
        holder.nav_title.setText(navtxt);
        holder.imageView.setImageResource(navIcons[position]);
    }

    @Override
    public int getItemCount() {
        return navlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nav_title;
        ImageView imageView;
        LinearLayout nav_item_container;

        public ViewHolder(final View itemView) {
            super(itemView);

            nav_title = (TextView) itemView.findViewById(R.id.nav_title);
            imageView = (ImageView) itemView.findViewById(R.id.menuicon);
            nav_item_container = (LinearLayout) itemView.findViewById(R.id.nav_item_container);

            nav_item_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (navigationItemClick != null) {
                        navigationItemClick.onClick(itemView, getAdapterPosition());
                    }
                }
            });

        }
    }

    public void setOnclickListner(NavigationItemClick navigationItemClick) {
        this.navigationItemClick = navigationItemClick;
    }

    public interface NavigationItemClick {
        void onClick(View v, int position);
    }
}
