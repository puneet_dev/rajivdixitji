package mobile.com.hello.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mobile.com.hello.R;
import mobile.com.hello.activities.WebsiteActivity;
import mobile.com.hello.models.RssModel;
import mobile.com.hello.util.JustTextView;

/**
 * Created by reflex on 22/8/17.
 */

public class ViewPagerDetailAdapter extends PagerAdapter {
    ArrayList<RssModel> postArrayList;
    TextView postTitle;
    String contentString;
    RssModel rssModel;
    Button visitWebsite;
    Context context;
    LinearLayout linearLayout;
    ImageView detailImage;
    com.emilsjolander.components.StickyScrollViewItems.StickyScrollView scrollView;
    int pagerPosition;
    DetailItemClick detailItemClick;

    public ViewPagerDetailAdapter(ArrayList<RssModel> postArrayList) {
        this.postArrayList = postArrayList;
        rssModel = new RssModel();
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(container.getContext());
        this.context = container.getContext();
        View view = layoutInflater.inflate(R.layout.detail_fragment, null);
        container.addView(view);
        this.pagerPosition = position;


        scrollView = (com.emilsjolander.components.StickyScrollViewItems.StickyScrollView) view.findViewById(R.id.ScrollView);


        rssModel = postArrayList.get(position);
        postTitle = (TextView) view.findViewById(R.id.detail_title);

        detailImage = (ImageView) view.findViewById(R.id.detail_image);

        final ImageView starred = (ImageView) view.findViewById(R.id.star_mark);
        if (rssModel.getStar() == 1) {
            starred.setImageResource(R.drawable.star_fill);
        } else {
            starred.setImageResource(R.drawable.star);
        }

        starred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detailItemClick != null) {
                    if (rssModel.getStar() == 1) {
                        //case 1
                        starred.setImageResource(R.drawable.star_fill);
                        detailItemClick.onClick(view, 0);
                    } else {
                        //case 0
                        starred.setImageResource(R.drawable.star);
                        detailItemClick.onClick(view, 1);

                    }

                }

            }
        });

        postTitle.setText(rssModel.getTitle());
        contentString = rssModel.getContent();
        contentString = contentString.replace("[\"The post\",\"appeared first on\",\".\"]", " ");
        contentString = contentString.replace("[", " ");
        contentString = contentString.replace("]", " ");
        contentString = replaceRegex("regex here", contentString, " ");
//        contentString= contentString.replace(".","\n");


        linearLayout = (LinearLayout) view.findViewById(R.id.linear_text);
        JustTextView justTextView = new JustTextView(context, contentString);
        linearLayout.addView(justTextView);

        if (rssModel.getImagePath() != null && !rssModel.getImagePath().equalsIgnoreCase("")) {
            Picasso.with(container.getContext())
                    .load(rssModel.getImagePath())
                    .into(detailImage);
        } else {
            Picasso.with(container.getContext())
                    .load(R.drawable.header_icon)
                    .error(R.drawable.header_icon)
                    .into(detailImage);
        }


        visitWebsite = (Button) view.findViewById(R.id.visitWebsite);
        visitWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rssModel = postArrayList.get(position);
                Intent intent = new Intent(context, WebsiteActivity.class);
                intent.putExtra("URL", rssModel.getPostLink());
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.start, R.anim.exit);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return postArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == object;

    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        postTitle = (TextView) view.findViewById(R.id.detail_title);
        linearLayout = (LinearLayout) view.findViewById(R.id.linear_text);
        container.removeView(view);

    }

    public String replaceRegex(String what, String string, String replace) {
        Pattern pattern = Pattern.compile(what);
        Matcher matcher = pattern.matcher(string);
        return matcher.replaceAll(replace);
    }

    public void changeImage() {
        /*if(temp){
            postArrayList.get(position).setIsClick("true");
            temp = false;
            notifyDataSetChanged();

        }else{
            postArrayList.get(position).setIsClick("false");
            temp = true;
            notifyDataSetChanged();
        }*/

    }


    public void setOnclickListner(DetailItemClick detailItemClick) {
        this.detailItemClick = detailItemClick;
    }

    public interface DetailItemClick {
        void onClick(View v, int value);
    }

}
