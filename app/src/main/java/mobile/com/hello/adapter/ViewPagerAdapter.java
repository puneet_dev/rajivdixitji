package mobile.com.hello.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import mobile.com.hello.fragments.PostFragment;
import mobile.com.hello.model.CategoryModel;
import mobile.com.hello.util.Constants;

/**
 * Created by reflex on 4/8/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public long getItemId(int position) {
        PostFragment.setFragmentPosition(position);
        return position;
    }

    @Override
    public Fragment getItem(int position) {
        PostFragment postFragment = null;

        if (position == 0) {
            postFragment = new PostFragment();
//            PostFragment.newInstance(CategoryModel.getInstance().getHealthList());

            if(CategoryModel.getInstance().getHealthList()!=null && CategoryModel.getInstance().getHealthList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getHealthList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/health/feed/", Constants.getInstance().healthString);
            }

        } else if (position == 1) {
            postFragment = new PostFragment();
//            postFragment.newInstance(CategoryModel.getInstance().getExposeList());

            if(CategoryModel.getInstance().getExposeList()!=null && CategoryModel.getInstance().getExposeList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getExposeList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/expose/feed/", Constants.getInstance().exposeString);
            }
        } else if (position == 2) {
            postFragment = new PostFragment();
//            PostFragment.newInstance(CategoryModel.getInstance().getHealthList());

            if(CategoryModel.getInstance().getHistoryList()!=null && CategoryModel.getInstance().getHistoryList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getHistoryList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/history/feed/", Constants.getInstance().realTruthString);
            }

        } else if (position == 3) {
            postFragment = new PostFragment();
//            PostFragment.newInstance(CategoryModel.getInstance().getHealthList());

            if(CategoryModel.getInstance().getNewsList()!=null && CategoryModel.getInstance().getNewsList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getNewsList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/news/feed/", Constants.getInstance().swadeshiString);
            }

        } else if (position == 4) {
            postFragment = new PostFragment();
//            PostFragment.newInstance(CategoryModel.getInstance().getHealthList());

            if(CategoryModel.getInstance().getRrgList()!=null && CategoryModel.getInstance().getRrgList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getRrgList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/rrg/feed/", Constants.getInstance().rightCallString);
            }

        } else if (position == 5) {
            postFragment = new PostFragment();

            if(CategoryModel.getInstance().getIncredibleList()!=null && CategoryModel.getInstance().getIncredibleList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getIncredibleList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/incredible-india/feed/", Constants.getInstance().incredibleString);
            }

        } else if (position == 6) {
            postFragment = new PostFragment();

            if(CategoryModel.getInstance().getOrganicFarmingList()!=null && CategoryModel.getInstance().getOrganicFarmingList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getOrganicFarmingList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/organic-farming/feed/", Constants.getInstance().organicString);
            }

        } else if (position == 7) {
            postFragment = new PostFragment();

            if(CategoryModel.getInstance().getHomeMadeList()!=null && CategoryModel.getInstance().getHomeMadeList().size()>0){
                postFragment.setData(CategoryModel.getInstance().getHomeMadeList());
            }else{
                postFragment.setData("http://rajivdixitji.com/category/homemade/feed/", Constants.getInstance().productString);
            }

        }
        return postFragment;
    }

    @Override
    public int getCount() {
        return 8;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Health";
        } else if (position == 1) {
            title = "Expose the Truth";
        } else if (position == 2) {
            title = "Real History";
        } else if (position == 3) {
            title = "Swadeshi News";
        } else if (position == 4) {
            title = "Right To Recall";
        } else if (position == 5) {
            title = "Incredible India";
        } else if (position == 6) {
            title = "Organic Farming";
        } else if (position == 7) {
            title = "Swadeshi Product";
        }

        return title;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
