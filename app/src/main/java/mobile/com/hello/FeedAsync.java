package mobile.com.hello;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import mobile.com.hello.entity.UserInfo;
import mobile.com.hello.interfaces.IAsyncTask;
import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.util.ResultSet;

import static android.content.ContentValues.TAG;

/**
 * Created by puneet on 25/7/17.
 */

public class FeedAsync extends AsyncTask<Object, String, ResultSet> {
    private String urlLink;
    FetchFeed fetchFeed;
    List<RssFeedModel> samples = null;
    UserInfo userInfo;
    IAsyncTask iAsyncTask;

    public FeedAsync(UserInfo usersInfo, IAsyncTask iAsyncTask) {
        this.userInfo = usersInfo;
        this.iAsyncTask = iAsyncTask;
    }

    @Override
    protected ResultSet doInBackground(Object... objects) {
        try {
            urlLink = userInfo.getUrl();
            URL url = new URL(urlLink);
            InputStream inputStream = url.openConnection().getInputStream();

            fetchFeed = new FetchFeed();
            samples = fetchFeed.parseFeed(inputStream);
            ResultSet resultSet = new ResultSet();
            resultSet.setResult(samples);

            return resultSet;


        } catch (IOException e) {
            Log.e(TAG, "Error", e);
        } catch (XmlPullParserException e) {
            Log.e(TAG, "Error", e);
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        iAsyncTask.OnPreExecute();
    }

    @Override
    protected void onPostExecute(ResultSet resultSet) {
        super.onPostExecute(resultSet);
        try{
            if(resultSet.getResult()!=null && resultSet.getResult().size()>0)
                iAsyncTask.OnPostExecute("Result", resultSet.getResult());
        }catch(NullPointerException e){
            e.printStackTrace();
        }

    }
}
