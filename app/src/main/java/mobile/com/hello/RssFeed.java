package mobile.com.hello;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobile.com.hello.asynctask.ConvertAsyncTask;
import mobile.com.hello.entity.UserInfo;
import mobile.com.hello.interfaces.IAsyncTask;
import mobile.com.hello.models.RssFeedModel;
import mobile.com.hello.models.RssModel;
import mobile.com.hello.util.Util;

public class RssFeed extends AppCompatActivity implements IAsyncTask {
    private Context context;
    List<String> rssEnteries = null;
    RecyclerView healthRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_feed);
        context = this;
        rssEnteries = new ArrayList<>();
        healthRecyclerView = (RecyclerView) findViewById(R.id.health_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        healthRecyclerView.setLayoutManager(linearLayoutManager);
        parseFeed();
    }

    private void parseFeed() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUrl("http://rajivdixitji.com/category/health/feed/");
        FeedAsync feedAsync = new FeedAsync(userInfo, this);
        feedAsync.execute();
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public Fragment getFragment() {
        return null;
    }

    @Override
    public void OnPreExecute() {
        Util.showDialog(context);
    }

    @Override
    public void OnPostExecute(String URL, List<RssFeedModel> result) {
        Util.dismissDialog();
        Log.d("result", result.toString());
        UserInfo userInfo = new UserInfo();
        userInfo.setResult(result);
        ConvertAsyncTask convertAsyncTask = new ConvertAsyncTask(userInfo, this);
        convertAsyncTask.execute();
    }

    @Override
    public void OnPostExecute(ArrayList<String> result) {

    }

    @Override
    public void OnPostExecute(String message, ArrayList<RssModel> result) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        /*RssAdapter rssAdapter = new RssAdapter(context, result,"Expose Real truth");
        healthRecyclerView.setAdapter(rssAdapter);*/
    }


    @Override
    public void OnErrorMessage(String Message) {

    }
}
