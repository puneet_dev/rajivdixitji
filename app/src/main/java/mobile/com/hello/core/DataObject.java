package mobile.com.hello.core;

import android.content.Context;
import android.database.Cursor;

import org.json.JSONObject;

import java.util.HashMap;


public class DataObject
{

	HashMap<String, Object> dataMap = null;
	public static String ID = "id";
	Context mContext;
	public DataObject(JSONObject object, Context ctxt)
	{
//		dataMap = parse(object);
	}

	public DataObject()
	{
		dataMap =  new HashMap<String, Object>();;
	}

	public DataObject(Cursor cursor, Context ctxt, String[] columnNames)
	{
		mContext = ctxt;
//		dataMap = parse(cursor, columnNames);
	}
	public void put(String key, Object value)
	{
		if(dataMap==null)
			dataMap =  new HashMap<String, Object>();;
		    dataMap.put(key,value);
	}

	public Object get(String key)
	{
		if(dataMap!=null)
		{
			return dataMap.get(key);
		}
		return null;
	}
	public void remove(String key)
	{
		if(dataMap!=null)
		{
			dataMap.remove(key);
		}

	}
	private HashMap<String, Object> parse(JSONObject object) {
		HashMap<String, Object> map = new HashMap<String, Object>();
/*
		
		@SuppressWarnings("unchecked")
		Set<Map.Entry<String, JsonElement>> set = object.entrySet();
	    Iterator<Map.Entry<String, JsonElement>> iterator = set.iterator();

		 HashMap<String, Object> map = new HashMap<String, Object>();

		 while(iterator.hasNext()) {
		        // loop to get the dynamic key
			Map.Entry<String, JsonElement> entry = iterator.next();
		    String keyname = (String)entry.getKey();
			Object temp= object.get(keyname);

			 if(temp!=null)
			 {
				 //value = temp.toString();

				 if(temp instanceof String) {
					 map.put(keyname,temp.toString());
				 }
				 else if(temp instanceof Long) {
				 	map.put(keyname,String.valueOf(temp));
			 	 }
				 else if(temp instanceof Integer) {
					 map.put(keyname,String.valueOf(temp));
				 }
				 else if(temp instanceof JSONArray) {

					 JSONArray jsdata = (JSONArray)temp;
					 String groupstring = "";
					 ArrayList<DataObject> tmpArray = new ArrayList<DataObject>();
					 for(int i=0;i<jsdata.size();i++)
					 {
						 Object tempobj = jsdata.get(i);
						 if(tempobj instanceof String) {
							 if (groupstring.isEmpty())
								 groupstring = (String) tempobj;
							 else
								 groupstring = groupstring + "|" + (String) tempobj;

							 }
						 else if(tempobj instanceof Integer ||
								 tempobj instanceof Character ||
								 tempobj instanceof Long ||
								 tempobj instanceof Double) {
							 if (groupstring.isEmpty())
								 groupstring = (String) String.valueOf(tempobj);
							 else
								 groupstring = groupstring + "|" + (String) String.valueOf(tempobj);

							 }
						 else if (tempobj instanceof JSONArray) {
							 *//*ArrayList<DataObject> tempdataObj = getDataObjectArrayFromJSON(secObj);
							 if(tempdataObj!=null) {
								 DataObject dt = new DataObject();
								 dt.put("" + i, tempdataObj);
								 Logger.e(AppGlobals.TAG_NAME_CORE, "Unsupported tags. A JSON ARRAY found inside JSON array with no Key name");
							 }*//*
						 }
						 else if (tempobj instanceof JSONArray){

						 }

						 else if (tempobj instanceof JSONObject) {
							 DataObject dt = new DataObject((JSONObject) jsdata.get(i), mContext);
							 tmpArray.add(dt);
						 }
					 }
					 if(!groupstring.isEmpty())
						 map.put(keyname, groupstring);
					 else
					 	map.put(keyname, tmpArray);
				 }
				 else if(temp instanceof JSONObject)
				 {
					 DataObject dt = new DataObject((JSONObject)temp,mContext );
					 map.put(keyname,dt);
				 }
			 }
			 else
				 map.put(keyname,"");

		 }*/
		 return map;
	}
	
	/*private  HashMap<String, Object> parse(Cursor cursor,String[] columnNames) {

		DatabaseHandler handler =  GeneralUtils.getInstance(mContext.getApplicationContext()).getDBHandle();
		HashMap<String, Object> map = null;
		if(cursor!=null)
		{
			String tvalue;
			map = new HashMap<String, Object>();
			for (int i = 0; i < columnNames.length; i++) {
					if (columnNames[i] != null) {
						try {
							tvalue = handler.getvaluefromCursor(cursor, columnNames[i]);
							if (tvalue != null) {
								map.put(columnNames[i],
										tvalue);
							} else {
								map.put(columnNames[i], "");
							}
						} catch (Exception e) {

						}
					}
				}
		}
			
		return map;
		
	}*/
	
}
